<?php 
use app\modules\cms\models\Page;
use yii\helpers\Html;
?>

<div class="col-md-1"></div>
  <?php foreach (Page::getParents() as $parentPage): ?>
      <div class="col-md-2">
          <h3><?= $parentPage->title; ?></h3>
          <?php foreach ($parentPage->getChilds()->all() as $page): ?>
              <?= Html::a($page->title, $page->getLink()); ?>
          <?php endforeach; ?>
      </div>
  <?php endforeach; ?>

  <div class="col-md-2">
      <h3>Panel klienta</h3>
      <a href="/site/login">Zaloguj</a>
      <a href="/client/register">Zalóż konto</a>
      <a href="/client/profile">Twoje konto</a>
      <a href="/shop/cart">Koszyk</a>
  </div>

<div class="col-md-4">
  <?= $this->render('_facebook'); ?>
</div>

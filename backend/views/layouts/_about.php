<?php 
use app\modules\cms\models\Box;
$box = Box::forceGet('about'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="img-noborder">
                <?php echo $box['content']; ?>
            </div>
        </div>
    </div>
</div>

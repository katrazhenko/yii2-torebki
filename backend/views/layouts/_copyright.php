<?php 
use bizley\cookiemonster\CookieMonster;
?>
<div class="container">
    <div class="row">
        <div class="clear20"></div>
        <div class="col-md-6 text-left">
            Copyright <?= date("Y"); ?> Tanie Torebki. All right reserved
        </div>
        <div class="col-md-6 text-right">
            Projektowanie zaawansowanych sklepów internetowych <a href="http://guido.com.pl">GUIDO</a>
        </div>
    </div>
</div>

<div class="clear20"></div>
<span id="toTop" onclick="$('html, body').animate({scrollTop: 0}, 200);"></span>
<?php $this->registerJs('
  $(window).scroll(function(){
     if( $(window).scrollTop() > 100 ) 
      $("#toTop").show();
      else
      $("#toTop").hide();
    });
'); ?>

<?php $this->registerJs(
  "$(document).delegate('*[data-toggle=\"lightbox\"]', 'click', function(event) {
        event.preventDefault();
        $(this).ekkoLightbox();
    }); "
); ?>

<?= CookieMonster::widget([
    'content' => [
        'buttonMessage' => 'OK', // instead of default 'I understand'
        'mainMessage' => 'Strona korzysta z plików cookies w celu realizacji usług i zgodnie z Polityką Plików Cookies. Możesz określić warunki przechowywania lub dostępu do plików cookies w Twojej przeglądarce.'
    ],
    'mode' => 'bottom'
])?>

<?php $this->endBody() ?>

<script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 882229760;
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
    /* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
    <img height="1" width="1" style="border-style:none;" alt=""
          src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/882229760/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

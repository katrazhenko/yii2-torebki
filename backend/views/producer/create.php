<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Producer */

$this->title = Yii::t('app', 'Dodaj');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Producenci'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="producer-create box box-success">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Setting */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="setting-form box-body">

    <?php $form = ActiveForm::begin(); ?>

    <?php if ($model->field_type == 'textarea'): ?>
        <?= $form->field($model, 'value')->textarea(['maxlength' => true]) ?>
    <?php else: ?>
        <?= $form->field($model, 'value')->textInput(['maxlength' => true]) ?>
    <?php endif; ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
    <?= $form->field($model, 'category')->textInput(['maxlength' => true]) ?>
    <div class="form-group">
        <?= Html::submitButton('Zapisz', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use backend\modules\cms\models\I18n;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\DeliverySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Dostawy');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="delivery-index box box-primary">


    <div class="box-header">
        <?= Html::a(Yii::t('app', 'Dodaj'), ['create'], ['class' => 'btn btn-sm btn-success pull-right']) ?>
    </div>
    <div class="box-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            'price',
            [
                'attribute'=>'pobranie',
                'format'=>'boolean',
                'filter'=>I18n::yesNo()
            ],
            'free',
            [
                'attribute'=>'active',
                'format'=>'boolean',
                'filter'=>I18n::yesNo()
            ],
            // 'info',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete}',
                'headerOptions' => ['width' => 90],
            ],
        ],
    ]); ?>
    </div>
</div>

<?php
use yii\helpers\Html;
use leandrogehlen\treegrid\TreeGrid;

$this->title = Yii::t('app', 'Kategorie');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-index box box-primary">
    <div class="box-header">
        <?= Html::a(Yii::t('app', 'Dodaj'), ['create'], ['class' => 'btn btn-sm btn-success pull-right']) ?>
    </div>


<?= TreeGrid::widget([
    'dataProvider' => $dataProvider,
    'keyColumnName' => 'id',
    'parentColumnName' => 'parent',
    'parentRootValue' => 0, //first parentId value
    'pluginOptions' => [
        'initialState' => 'collapsed',
    ],
    'columns' => [
        'title',
        'id',
        [
            'attribute' => 'active',
            'format' => 'boolean',
        ],
        ['class' => 'yii\grid\ActionColumn']
    ]
]); ?>

</div>

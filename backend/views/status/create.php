<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Status */

$this->title = Yii::t('app', 'Create Status');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Statuses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="status-create box box-success">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ClientSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Zaimportowane produkty');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="client-index box box-primary">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="box-header">
        <?= Html::a(Yii::t('app', 'Importuj'), ['import'], ['class' => 'btn btn-sm btn-success pull-right']) ?>
    </div>
    <div class="box-body">
        <h4><?= Yii::t('app', 'Ilość zaimportowanych produktów') ?>: <?= $countProduct ?></h4>
        <div class="row">
            <div class="col-md-12">
                <?php
                foreach ($data as $d) {
                    
var_dump($d['file']);
                    ?>
                    <div class="row">
                        <div class="col-md-3">
                            <span><?= Yii::t('app', 'Nazwa') ?>: <?= $d['name'] ?></span>
                        </div>
                        <div class="col-md-3">
                            <span><?= Yii::t('app', 'Active') ?>: <?= $d['active'] ?></span>
                        </div>
                        <div class="col-md-3">
                            <?= Yii::t('app', 'Ilość') ?> <?= $d['quantity'] ?>
                        </div>
                        <div class="col-md-3">
                            <?= $d['type'] ?> <?= $d['wyswietlenie'] ?>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
</div>

<?php

use common\models\Status;
use yii\bootstrap\ActiveForm;
use yii\data\ArrayDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Order */

$this->title = '#' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Zamówienia'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-primary">
    <div class="order-view box-body">
        <p>
            <?= Html::a(Yii::t('app', 'Drukuj'), ['print', 'id' => $model->id], ['target'=>'_blank','class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Edytuj'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Usuń'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Czy napewno ?'),
                    'method' => 'post',
                ],
            ]) ?>
        </p>
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Status
                </div>
                <div class="panel-body">
                    <?php $form = ActiveForm::begin(); ?>
                    <?= $form->field($model, 'status')->dropDownList(Status::getFilterList()) ?>
                    <div class="form-group">
                        <?= Html::submitButton('Zmień status', ['class' => 'btn btn-success']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    Produkty
                </div>
                <div class="panel-body">
                    <?= GridView::widget([
                        'dataProvider' => new ArrayDataProvider([
                            'allModels' => $model->items,
                        ]),
                        'filterModel' => false,
                        'columns' => [
                            'name',
                            'price_one',
                            'quantity',
                            'options',
                        ],
                    ]); ?>
                </div>
            </div>
        </div>


        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Dane zamówienia
                </div>
                <div class="panel-body">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id',
                            'date',
                            'statusName',
                            'products_price',
                            'transport_price',
                            'transport_name',
                            'transport_number',
                            'payment_price',
                            'payment_name',
                            'payment_number',
                            'notice',
                        ],
                    ]) ?>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    Rabat
                </div>
                <div class="panel-body">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'rabat_name',
                            'rabat',
                            'voucher_price',
                            'voucher_percent',
                            'voucher_name',
                        ],
                    ]) ?>
                </div>
            </div>

        </div>

        <div class="col-md-6">


            <div class="panel panel-default">
                <div class="panel-heading">
                    Użytkownik
                </div>
                <div class="panel-body">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'c_name',
                            'c_email:email',
                            'c_phone',
                        ],
                    ]) ?>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    Dane do dostawy
                </div>
                <div class="panel-body">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'd_company',
                            'd_name',
                            'd_address',
                            'd_postcode',
                            'd_city',
                            'd_country',
                            'd_phone',
                        ],
                    ]) ?>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    Dane do faktury
                </div>
                <div class="panel-body">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'fv:boolean',
                            'fv_nip',
                            'fv_company',
                            'fv_address',
                            'fv_postcode',
                            'fv_city',
                            'fv_country',
                        ],
                    ]) ?>
                </div>
            </div>
        </div>

    </div>

</div>


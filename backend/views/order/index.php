<?php

use common\models\Client;
use common\models\Status;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Zamówienia');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-index box box-primary">

    <div class="box-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                'id',
                'date',
                [
                    'attribute' => 'client_id',
                    'value' => 'client.toString',
                ],
                [
                    'attribute' => 'status',
                    'value' => 'statusName',
                    'filter' => Status::getFilterList()
                ],
                'products_price',
                'transport_price',
                // 'transport_name',
                // 'transport_number',
                // 'payment_price',
                // 'payment_name',
                // 'payment_number',
                // 'notice',
                //
                // 'fv:boolean',
                // 'fv_nip',
                // 'fv_company',
                // 'fv_address',
                // 'fv_postcode',
                // 'fv_city',
                // 'fv_country',
                // 'd_name',
                // 'd_company',
                // 'd_address',
                // 'd_postcode',
                // 'd_city',
                // 'd_country',
                // 'd_phone',
                // 'c_name',
                // 'c_surname',
                // 'c_email:email',
                // 'c_phone',
                // 'rabat_name',
                // 'rabat',
                // 'voucher_price',
                // 'voucher_percent',
                // 'voucher_name',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view}',
                    'headerOptions' => ['width' => 90],
                ],
            ],
        ]); ?>
    </div>
</div>

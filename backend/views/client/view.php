<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Client */

$this->title = $model->__toString();
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Klienci'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-primary">
<div class="client-view box-body">

    <p>
        <?= Html::a(Yii::t('app', 'Edytuj'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Usuń'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                Użytkownik
            </div>
            <div class="panel-body">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'login',
                        'date',
                        'c_name',
                        'c_email:email',
                        'c_phone',
                        'type',
                        'rabat',
                    ],
                ]) ?>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                Dane do faktury
            </div>
            <div class="panel-body">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'fv:boolean',
                        'fv_nip',
                        'fv_company',
                        'fv_address',
                        'fv_postcode',
                        'fv_city',
                        'fv_country',
                    ],
                ]) ?>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                Dane do dostawy
            </div>
            <div class="panel-body">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'd_company',
                        'd_name',
                        'd_address',
                        'd_postcode',
                        'd_city',
                        'd_country',
                        'd_phone',
                    ],
                ]) ?>
            </div>
        </div>
    </div>


</div>
</div>
<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ClientSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Klienci');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="client-index box box-primary">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="box-header">
        <?= Html::a(Yii::t('app', 'Dodaj klienta'), ['create'], ['class' => 'btn btn-sm btn-success pull-right']) ?>
    </div>
    <div class="box-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'c_name',
            'c_email:email',
            'fv:boolean',
            'rabat',
            'c_phone',
            // 'fv_address',
            // 'fv_postcode',
            // 'fv_city',
            // 'fv_country',
            // 'd_company',
            // 'd_name',
            // 'd_address',
            // 'd_postcode',
            // 'd_city',
            // 'd_country',
            // 'd_phone',
            //
            //
            //
            // 'c_phone',
            // 'password',
            //
            //

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete}',
                'headerOptions' => ['width' => 90],
            ],
        ],
    ]); ?>
    </div>
</div>

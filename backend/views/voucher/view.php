<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Voucher */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Vouchery', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-primary">
    <div class="voucher-view box-body">

        <p>
            <?= Html::a('Edytuj', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Usuń', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        </p>
        <div class="clearfix"></div>
        <div class="col-md-6">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'price',
                    'percent',
                    'code',
                    'desc',
                    'quantity',
                    'date_from',
                    'date_to',
                    'client',
                    'mail',
                ],
            ]) ?>
        </div>
        <div class="col-md-6">
            <?php foreach($model->codes as $code): ?>
                <?= $code->code; ?><br/>
            <?php endforeach; ?>
        </div>
    </div>
</div>
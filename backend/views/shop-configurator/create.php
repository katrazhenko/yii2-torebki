<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ShopConfigurator */

$this->title = Yii::t('app', 'Create Shop Configurator');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Shop Configurators'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shop-configurator-create box box-success">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

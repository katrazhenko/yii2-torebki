<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Product */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Products'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-primary">
<div class="product-view box-body">

    <p>
        <?= Html::a(Yii::t('app', 'Edytuj'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Usuń'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'product_id',
            'part_number',
            'category_id',
            'name',
            'description:ntext',
            'price_import',
            'price_basic',
            'price',
            'promotion',
            'bargain',
            'recommended',
            'bestseller',
            'new',
            'quantity',
            'producer',
            'warranty',
            'type',
            'czas_dostawy',
            'vat',
            'photo',
            'active',
            'open',
            'mark',
            'konfigurator:ntext',
            'widget_offer',
            'widget_recommend:boolean',
            'widget_price:boolean',
            'update_done',
            'update_new',
        ],
    ]) ?>

</div>
</div>
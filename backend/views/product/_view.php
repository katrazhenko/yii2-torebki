<div class="<?= isset($class) ? $class : 'col-xs-3 product-page-box text-center'; ?>">
    <div class="padding-product">
        <a href="<?= $model->url(); ?>" class="img-block">
            <?= $model->img(); ?>
        </a>
        <a class="title" href="<?= $model->url(); ?>">
            <?= mb_substr($model->name, 0, 28) . (mb_strlen($model->name) > 28 ? '...' : ''); ?>
        </a>
        <p class="product-price">
            <?php if($model->isPromo()): ?>
            <span><?= $model->basicPriceBrutto(); ?></span>
            <?php endif; ?>
            <?= $model->priceBrutto(); ?>
        </p>

        <div class="clearfix"></div>
        <div class="category">
            <?= $model->categoryName; ?>
        </div>
    </div>
    <a class="button" href="/shop/add?id=<?= $model->id; ?>">Do koszyka</a>
</div>

<?php
use frontend\modules\cms\models\Box;

?>
<div class="container">
    <!-- breadcrump -->
    <div class="breadcrumb">

        <a href="/">Strona Główna</a>
        &raquo;
        <?php
        foreach ($model->breadcrumbs() as $bcategory): ?>
            <a href="<?= $bcategory->getLink(); ?>"><?= $bcategory->title; ?></a>
            &raquo;
        <?php endforeach; ?>

        <a href="#"><?= $model->name; ?></a>
    </div>

    <div class="product-panel">
        <div class="row">
            <div class="col-sm-4">
                <!-- image preview - modal window -->
                <div id="myModal" class="modal">
                    <span class="close" onclick="document.getElementById('myModal').style.display='none'">&times;</span>
                    <img class="modal-content" id="img01">
                    <div id="caption"></div>
                </div>

                <div class="main-photo">
                    <a href="<?= $model->getUrl('1024x600'); ?>" data-gallery="multiimages" data-toggle="lightbox" data-title="<?= $model->name; ?>">
                        <img id="myImg" src="<?= $model->getUrl('512x512'); ?>" class="img-responsive" alt="<?= $model->name; ?>">
                    </a>
                </div>

                <div class="thumbnails">
                    <?php foreach ($model->images as $image): ?>
                        <a href="<?= $image->getUrl('1024x600'); ?>" data-gallery="multiimages" data-toggle="lightbox" data-title="<?= $model->name; ?>">
                            <img id="myImg" src="<?= $image->getUrl('250x250'); ?>" width="64" class="img-responsive" alt="<?= $model->name; ?>">
                        </a>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="col-sm-7" style="margin-left: 10px;">
                <div class="col-sm-8">
                    <div class="title"><?= $model->name; ?></div>
                    <div class="product-id">ID produktu: <?= $model->product_id; ?></div>
                    <?php /*
                    <div class="rating">
                        <div class="row">
                            <div class="col-sm-4">
                                <select id="example">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </select>
                            </div>
                            <div class="col-sm-4">
                                <a href="#">13 osób (opinie)</a>
                            </div>
                        </div>
                    </div>
*/ ?>
                    <div>
                        <b><?= $model->producer ? 'Producent: ' . $model->producer->name : ''; ?></b>
                        Typ: <b><?= current($model->breadcrumbs())->title; ?></b>
                    </div>
                </div>
                <!--<div class="col-sm-2">
                    <img src="/img/logo_dell.png" class="img-responsive"/>
                </div>-->
            </div>
            <?php if ($model->priceBrutto() > 0 and $model->quantity > 0): ?>
                <form class="form-inline" action="/shop/add?id=<?= $model->id; ?>" method="post">
                    <div class="col-sm-7">

                        <div class="row">
                            <div class="price-panel">
                                <div class="col-sm-6">
                                    <?php if ($model->isPromo()): ?>
                                        <div class="old-price"><?= $model->basicPriceBrutto(); ?> PLN</div>
                                    <?php endif; ?>
                                    <div class="price mainprice" data-price="<?= $model->priceBrutto(); ?>">
                                        <?= $model->priceBrutto(); ?> <span>PLN</span><br/>
                                        <?php if (empty($model->getKonfigurator())): ?>
                                            <small class="text-muted" style="font-size:12px">NETTO: <?= $model->priceNetto(); ?> PLN</small>
                                        <?php endif; ?>
                                    </div>
                                    <div class="btn btn-success" id="rata" onclick="PoliczRate(<?= $model->priceBrutto(); ?>);">Oblicz rate</div>
                                </div>
                                <?php $this->registerJs('


                                var price = 0;
                                $(".options").change(function(){
                                    price=0;
                                    $(".options :selected").each(function(){
                                        price += $(this).data("price");
                                    });
                                    $(".mainprice").html( ( $(".mainprice").data("price")*1 + price*1).toFixed(2) +" <span>PLN</span>");
                                });
                                
                                '); ?>
                                <div class="col-sm-6">

                                    <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>"/>
                                    <div class="form-group">
                                        <input type="text" name="q" value="1" class="form-control">
                                    </div>
                                    <button type="submit" class="btn btn-default">Do koszyka</button>

                                    <br>
                                    Dostępność produktu: <span class="text-warning">tylko <?= $model->quantity; ?> sztuk</span>
                                    <div class="clear20"></div>

                                    <?php if ($model->getKonfigurator() and !$model->product_id): ?>
                                        <strong>SKONFIGURUJ KOMPUTER</strong>
                                        <div class="clear10"></div>
                                        <?php foreach ($model->konfiguratorObjects() as $type => $k): ?>
                                            <div class="row">
                                                <div class="col-md-3"><label><?= $type; ?></label></div>
                                                <div class="col-md-9">
                                                    <select name="additional[]" class="form-control options">
                                                        <option data-price="0">standard</option>
                                                        <?php foreach ($k as $r): ?>
                                                            <option value="<?= $r->id; ?>" data-price="<?= $r->price; ?>">
                                                                <?= $r->value; ?> (+<?= $r->price; ?>)
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="clear10"></div>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="clear20"></div>
                        <div class="col-sm-11 col-sm-offset-1">
                            Koszty dostawy: <b>30 zł - po wpłacie na konto* nie dotyczy towarów wysyłanych na palecie/ wielkogabarytowych </b><br>
                            Dostawa w ciągu: <b>3-5 dni roboczych</b><br>
                            Odbiór osobisty
                        </div>
                    </div>
                </form>
            <?php else: ?>
                <div class="col-sm-7 price-panel">
                    <div class="row">
                        <div class="col-sm-6">

                        </div>
                        <div class="col-sm-6">
                            <a href="/pl/page/kontakt" class="btn"><img src="/images/zapytaj.png"/></a>
                            <br>
                            Dostępność produktu: <span class="text-warning">tylko <?= $model->quantity; ?> sztuk</span>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <div class="col-md-7 col-md-offset-5">
                <div class="row">

                    <div class="col-sm-6">
                        <!--                        <a href="#" style="color: #666666;">Dodaj do schowka</a><br>-->
                        <!--                        <a href="#" style="color: #666666;">Zapytaj o produkt</a>-->
                    </div>
                </div>

            </div>
        </div>

        <!-- tab menu -->
        <div class="tabs">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a data-toggle="tab" href="#description">Opis</a>
                </li>
                <li>
                    <a data-toggle="tab" href="#cechy">Specyfikacja</a>
                </li>
                <!--                <li>-->
                <!--                    <a data-toggle="tab" href="#reviews">Opinie</a>-->
                <!--                </li>-->
                <li>
                    <a data-toggle="tab" href="#guaranty">Gwarancja</a>
                </li>
                <?php /*     <li>
                    <a data-toggle="tab" href="#accessories">Akcesoria do tego produktu</a>
                </li>
                <li>
                    <a data-toggle="tab" href="#maintenance">Jak konserwujemy produkty</a>
                </li>*/ ?>
            </ul>

            <div class="tab-content">
                <div id="description" class="tab-pane fade in active">
                    <p><?= $model->description; ?></p>
                </div>
                <div id="cechy" class="tab-pane fade">
                    <table class="table table-condensed table-striped">
                        <?php foreach ($model->productAttributesSort as $attribute): ?>
                            <tr>
                                <td><?= $attribute->attributeName->name; ?></td>
                                <td><?= $attribute->value; ?> <?= $attribute->unit; ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <div id="reviews" class="tab-pane fade">
                    <p>Opinie</p>
                </div>
                <div id="guaranty" class="tab-pane fade">
                    <?= $model->guarantee ? $model->guarantee->name : ''; ?>
                </div>
                <div id="accessories" class="tab-pane fade">
                    <p>Akcesoria</p>
                </div>
                <div id="maintenance" class="tab-pane fade">
                    <p><?= Box::forceGet('konserwacja')->content; ?></p>
                </div>
            </div>
        </div>

        <!--        <div class="buttons">-->
        <!--            <img src="/img/polec_znajomym.png" class="img-responsive"/>-->
        <!--            <a href="#">Poleć znajomym</a>-->
        <!---->
        <!--            <img src="/img/drukuj.png" class="img-responsive"/>-->
        <!--            <a href="#">Wersja do druku</a>-->
        <!---->
        <!--            <img src="/img/zglos_blad.png" class="img-responsive"/>-->
        <!--            <a href="#">Zgłoś błąd</a>-->
        <!--        </div>-->
        <?php if ($suggested): ?>
        <div class="promotion">
            <div class="title">
                <h4 class="beauty">
                Proponowane produkty
                </h4>
            </div>
            

            <div class="product-list">
                <div class="container-fluid">
                    <div class="row">
                        <?php foreach ($suggested as $product): ?>
                            <div class="col-md-6 col-lg-3">
                                <a href="<?= $product->url(); ?>">
                                    <?= $product->img(['style' => "width: 128px; height: 128px;", 'class' => "img-responsive"]); ?>
                                    <b>
                                        <?= mb_substr($product->name, 0, 28) . (mb_strlen($product->name) > 28 ? '...' : ''); ?>
                                    </b>
                                    <div class="price">
                                        <?php if ($product->isPromo()): ?>
                                            <span><?= $product->basicPriceBrutto(); ?></span>
                                        <?php endif; ?>
                                        <?= $product->priceBrutto(); ?> zł
                                    </div>
                                    <span style="color: gray; font-size: 11px;"><?= $product->category->title; ?></span>
                                </a>
                            </div>
                        <?php endforeach; ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php endif; ?>
</div>
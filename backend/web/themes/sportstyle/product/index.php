<?php

use yii\widgets\ListView;
use yii\helpers\Html;
use yii\widgets\Pjax;
?>
</div>
<div class="bg-category">
    <div class="container">
        <!-- category description -->
        <div class="category-info">
            <div class="col-md-6 text-justify">
                <h3><?= $category->title; ?></h3>
                <?= $category->description; ?>
            </div>

            <div class="col-md-6">
                <form action="" method="post" id="ffilter">
                    <div class="clear20"></div>
                    <div class="col-xs-3">
                        <labeL>Sortuj:</labeL>
                    </div>
                    <div class="col-xs-9">

                        <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>"/>
                        <select class="form-control" name="sort" onchange="$('#ffilter').submit()">
                            <option value="newest" <?= (Yii::$app->session->has('sort-list') && Yii::$app->session->get('sort-list') == 'newest') ? 'selected="selected"' : '' ?>>Od najnowszych</option>
                            <option value="oldest" <?= (Yii::$app->session->has('sort-list') && Yii::$app->session->get('sort-list') == 'oldest') ? 'selected="selected"' : '' ?>>Od najstarszych</option>
                            <option value="priceA" <?= (Yii::$app->session->has('sort-list') && Yii::$app->session->get('sort-list') == 'priceA') ? 'selected="selected"' : '' ?>>Od najtańszych</option>
                            <option value="priceZ" <?= (Yii::$app->session->has('sort-list') && Yii::$app->session->get('sort-list') == 'priceZ') ? 'selected="selected"' : '' ?>>Od najdroższych</option>
                            <option value="nameA" <?= (Yii::$app->session->has('sort-list') && Yii::$app->session->get('sort-list') == 'nameA') ? 'selected="selected"' : '' ?>>Od A-Z</option>
                            <option value="nameZ" <?= (Yii::$app->session->has('sort-list') && Yii::$app->session->get('sort-list') == 'nameZ') ? 'selected="selected"' : '' ?>>Od Z-A</option>
                        </select>

                    </div>
                    <div class="clear10"></div>
                    <div class="col-xs-3">
                        <labeL>Wyświetl:</labeL>
                    </div>
                    <div class="col-xs-9">
                        <select class="form-control" name="type" onchange="$('#ffilter').submit()">
                            <option value="all" <?= (Yii::$app->session->has('type-list') && Yii::$app->session->get('type-list') == 'all') ? 'selected="selected"' : '' ?>>Wszystkie</option>
                            <option value="new" <?= (Yii::$app->session->has('type-list') && Yii::$app->session->get('type-list') == 'new') ? 'selected="selected"' : '' ?>>Nowości</option>
                            <option value="recommend" <?= (Yii::$app->session->has('type-list') && Yii::$app->session->get('type-list') == 'recommend') ? 'selected="selected"' : '' ?>>Polecane</option>
                        </select>
                    </div>
                </form>
            </div>
            <div class="clear20"></div>
        </div>
    </div>
</div>
<div class="container">
    <!-- breadcrump -->
    <div class="breadcrumb">
        <a href="/">Strona Główna</a>
        &raquo;
        <a href="<?= $category->getLink(); ?>"><?= $category->title; ?></a>
    </div>

    <div id="wrapper">

        <!-- left sidebar -->
        <div id="sidebar-wrapper" class="sidebar-filters">
            <form method="get" action="" id="filter-form">
                <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>"/>
                <ul class="sidebar">
                    <div style="height: 15px"></div>
                    <li class="sidebar-header">
                        <h4 class="beauty">Kategorie</h4>                      
                    </li>
                    <?php if ($category->parentCategory and $category->parentCategory->parent != 0): ?>
                        <li>
                            <a href="<?= $category->parentCategory->getLink(); ?>" class="active">&raquo; <?= $category->parentCategory->title; ?></a>
                        </li>
                    <?php endif; ?>
                    <?php foreach ($category->getChildren()->where(['active' => 1])->all() as $subcategory): ?>
                        <li>
                            <a href="<?= $subcategory->getLink(); ?>" class="active"><?= $subcategory->title; ?></a>
                        </li>
                    <?php endforeach; ?>


                    <?php if ($category->filters): ?>
                        <li style="border-top: 1px solid silver; box-shadow: 0px -4px 4px #e6e6e6; margin-top: 10px;">
                            <div style="height: 15px"></div>
                            <h4>Filtrowanie</h4>
                            <hr class="sidebar-header-delimiter"/>
                        </li>
                        <li class="remove-filters" style="padding: 0px 20px 0px 20px; text-align: right">

                        </li>

                        <?php $keys = is_array($searchModel->attributes) ? array_keys($searchModel->attributes) : [];
                        ?>
                        <div>
                            <?php foreach ($category->getFiltersWithName() as $filter): ?>
                                <div class="system-filter">
                                    <li class="filter-name">
                                        <b><?= $filter['name']; ?></b>
                                    </li>
                                    <li class="filter-content">
                                        <?php foreach ($filter_option[$filter['id']] as $option): ?>
                                            <?= Html::checkbox('ProductSearch[attributes][' . $filter['id'] . '][' . $option['value'] . ']', in_array($filter['id'], $keys) && isset($searchModel->attributes[$filter['id']][$option['value']])) . ' ' . $option['value']; ?><br/>
                                        <?php endforeach; ?>

                                    </li>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <li>
                            <b>Cena (zł)</b><br>
                            <div class="col-sm-5">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Od" value="<?= $searchModel->price_from; ?>" name="ProductSearch[pricem]"/>
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Do" name="ProductSearch[price_to]"/>
                                </div>
                            </div>
                        </li>
                        <!--                        <li class="text-center">
                                                    <button class="btn-danger btn">Filtruj</button>
                                                </li>-->
                        <li class="remove-filters" style="padding: 0px 20px 20px 20px; text-align: right">

                        </li>
                    <?php endif; ?>
                    <?php /*

                      <div id="manufacturer-filter">
                      <li>
                      <b>Producent</b>
                      </li>
                      <li>
                      <a href="#" class="active">Lenovo [100]</a>
                      </li>
                      <li>
                      <a href="#">Asus [100]</a>
                      </li>
                      <li>
                      <a href="#">Dell [100]</a>
                      </li>
                      <li>
                      <a href="#">HP [100]</a>
                      </li>
                      <li>
                      <a href="#">MSI [100]</a>
                      </li>
                      </div>

                      <div id="system-filter">
                      <li>
                      <b>System</b>
                      </li>
                      <li>
                      <a href="#">Windows 10</a>
                      <a href="#" class="active">Brak systemu operacyjnego</a>
                      <a href="#">Windows 8</a>
                      <a href="#">Windows 10</a>
                      </li>
                      </div>
                      <div id="cpu-filter">
                      <li>
                      <b>Procesor</b>
                      </li>
                      <li>
                      <a href="#">Intel Core i5</a>
                      <a href="#">Intel Core i7</a>
                      <a href="#">Intel Core i3</a>
                      <a href="#">Intel Pentium</a>
                      <a href="#" class="active">Intel Celeron</a>
                      </li>
                      </div>
                      <div id="gpu-filter">
                      <li>
                      <b>Karta graficzna</b>
                      </li>
                      <li>
                      <a href="#" class="active">Dedykowana</a>
                      <a href="#">Zintegrowana</a>
                      <a href="#">Układ hybrydowy</a>
                      </li>
                      </div>
                      <li>
                      <b>Pamięć RAM (GB)</b><br>
                      <div class="col-sm-5">
                      <div class="input-group">
                      <input type="text" class="form-control" placeholder="Od">
                      </div>
                      </div>
                      <div class="col-sm-5" style="margin-bottom: 30px;">
                      <div class="input-group">
                      <input type="text" class="form-control" placeholder="Do">
                      </div>
                      </div>
                      </li>
                     */ ?>
                </ul>
            </form>
        </div>

        <!-- product list -->
        <div class="product-list">
            <div class="container-fluid">
                <div class="row">
                    <?php Pjax::begin(['id' => 'pjax-product-list', 'timeout' => false, 'enablePushState' => true, 'enableReplaceState' => true, 'clientOptions' => []]); ?>
                    <?=
                    ListView::widget([
                        'id' => 'product-list',
                        'dataProvider' => $dataProvider,
                        'itemOptions' => ['class' => 'view'],
                        'itemView' => '_view',
                        'layout' => "{items}<div class='clearfix'></div>{pager}",
                    ])
                    ?>
                    <?php Pjax::end(); ?>
                </div>
            </div>
        </div>
    </div>

</div>
<div>
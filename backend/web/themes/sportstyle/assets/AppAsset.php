<?php

namespace frontend\themes\sportstyle\assets;

use Yii;
use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle {

    public function init() {
        $this->sourcePath = Yii::$app->view->theme->basePath . '/assets';
        parent::init();
    }

    public $css = [
        
        'https://fonts.googleapis.com/css?family=Lato:100,300,400,700|Rajdhani:300,400,500,700',
        'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css',
        'css/yamm.css',
        'css/ekko-lightbox.min.css',
        'css/main.less',
    ];
    public $js = [
        'js/can.custom.js',
        'js/main.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\web\YiiAsset',
        //'raoul2000\bootswatch\BootswatchAsset',
        'yii\bootstrap\BootstrapAsset',
        //'raoul2000\bootswatch\BootswatchAsset',
        'yii\jui\JuiAsset',
        //'yii\bootstrap\BootstrapPluginAsset'
    ];

}

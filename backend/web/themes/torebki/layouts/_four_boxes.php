<?php

use frontend\modules\cms\models\Box;
use yii\helpers\Html;
$countBox=3;
?>
<div class="container start-box">
    <div class="row">
        <?php for ($i = 1; $i <= $countBox; $i++): ?>
            <div class="col-md-<?= (int)12/$countBox ?> col-sm-6 col-xs-12 static-box-rwd">
                <?php $box = Box::forceGet('home' . $i, 100) ?>
                <div class="box box-<?php echo $i; ?>">

                        <?php
                        //$content = '';
                        //$content .= $box['title'];
                        $content = Html::img($baseUrl . '/images/smallbaner' . $i . '.png', ['class' => 'img-responsive', 'style'=>"width:100%", 'alt' => $box['title']]);
                        //$content .= Html::a('<i class="fa fa-chevron-right" aria-hidden="true"></i>', $box['link'], ['class' => 'btn btn-success']);
                        echo Html::a($content, $box['link']); ?>
                    
                    <div class="desc">
                        <?= $box['content']; ?>
                    </div>
                </div>
            </div>
        <?php endfor; ?>
    </div>
</div>
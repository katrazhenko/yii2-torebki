<?php

namespace app\controllers;

use app\models\Delivery;
use app\models\Order;
use app\models\OrderItem;
use app\models\VoucherCode;
use app\models\Basket;
use Yii;
use yii\web\Controller;

/**
 * Site controller
 */
class ShopController extends Controller {

    public function actionCard() {
        $basket = new Basket();

        if ($basket->isEmpty()) {
            return $this->render('card-empty');
        } else {
            $deliveryModel = Delivery::find()->where(['active'=>1])->all();

            $delivery = Delivery::findOne($basket->getTransportId());

//            var_dump($basket->getTransportId()); //10
//            var_dump($delivery); //null


            $payments = [];
            if ($delivery && empty($delivery->payment_method) === false) {
                $payments = unserialize($delivery->payment_method);
            }

            return $this->render('card', [
                        'deliveryModel' => $deliveryModel,
                        'basket' => $basket,
                        'payments' => $payments
            ]);
        }
    }

    public function actionConfirm() {

        $basket = new Basket();

        if ($basket->isEmpty()) {
            return $this->render('card-empty');
        }

        if (isset($_POST['notice'])) {
            $basket->setNotice($_POST['notice']);
        }

        if (isset($_POST['voucher'])) {

            if (!$basket->setVoucher($_POST['voucher-id'])) {
                Yii::$app->getSession()->setFlash('error', implode('<br/>', $basket->error));
            } else {
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Kupon rabatowy prawidłowy'));
            }
            return $this->redirect(['card']);
        }

        if (!$basket->getTransportId() || !$basket->getPaymentId()) {
            Yii::$app->getSession()->setFlash('error', Yii::t('app', 'Wybierz sposób dostawy oraz rodzaj płatności'));
            return $this->redirect(['card']);
        }

        if (Yii::$app->user->isGuest) {
            if ($basket->noClient()) {
                return $this->redirect(['site/login', 'back' => '/shop/confirm']);
            }
        }

        return $this->redirect('order');
        // return $this->render('card-confirm', ['basket' => $basket]);
    }

    public function actionOrder() {
        $basket = new Basket();
        if ($basket->isEmpty()) {
            return $this->render('card-empty');
        }

        $order = new Order();
        $order->copyClient();
        $order->status = 0;
        $order->products_price = $basket->productsPrice();
        $order->transport_price = $basket->getTransportCost();
        $order->transport_name = $basket->getTransportName();
        $order->payment_name = $basket->getPaymentId();
        $order->notice = $basket->getNotice();
        if ($basket->isVoucher()) {
            $order->voucher_price = $basket->getVoucherPrice();
            $order->voucher_percent = $basket->getVoucherPercent();
            $order->voucher_name = $basket->getVoucherCode();
        }
        $order->save(false);

        VoucherCode::updateAll(['order_id' => $order->id], 'code="' . $order->voucher_name . '"');

        foreach ($basket->getItems() as $item) {
            $orderItem = new OrderItem();
            $orderItem->order_id = $order->id;
            $orderItem->product_id = $item['attributes']['product_id'];
            $orderItem->name = $item['product']->name;
            $orderItem->price_one = $item['product']->priceBrutto() + $item['add_price'];
            $orderItem->quantity = $item['attributes']['quantity'];
            $orderItem->options = json_encode($item['attributes']['options']);
            $orderItem->save(false);
        }
        $order->sendConfirmMail();
        $basket->remove();
        if ($order->payment_name == 'online') {
            return $this->render('payu', ['basket' => $basket, 'model' => $order]);
        } else {
            $_SESSION['order_id'] = $order->id;
            return $this->render('thank-you', ['order_id' => $order->id]);
        }
    }

    public function actionTest() {
        return $this->render('payu', ['model' => Order::findOne(3)]);
    }

    public function actionThankyou()
    {
        return $this->render('thank-you');
    }


    public function actionPrzelewy24() {

        return $this->render('thank-you', ['order_id' => $_SESSION['order_id']]);
    }

    public function actionAdd($id) {
        $basket = new Basket();
        if (!$error = $basket->add($id)) {
            return $this->redirect(['card']);
        } else {
            Yii::$app->getSession()->setFlash('error', $basket->error);
            return $this->redirect($error['link']);
        }
    }

    public function actionDelete($key) {
        $basket = new Basket();
        $basket->delete($key);
        return $this->redirect(['card']);
    }

    public function actionChange($key, $quantity) {
        $basket = new Basket();
        $basket->change($key, $quantity);
        return $this->redirect(['card']);
    }

    public function actionTransport($id) {
        $basket = new Basket();
        $basket->setTransport($id);
        return $this->redirect(['card']);
    }

    public function actionPayment($id) {
        $basket = new Basket();
        $basket->setPayment($id);
        return $this->redirect(['card']);
    }

}

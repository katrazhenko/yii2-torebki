<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use common\models\Producer;
use common\models\Product;
use common\models\ShopProductImage;
use Intervention\Image\Exception\NotReadableException;
use Intervention\Image\ImageManager;
use League\Uri\Parser;

ini_set("memory_limit", "4000M");
ini_set('max_execution_time', 55000);

class ImportController extends Controller {

    /**
     * Import produktów
     */
    public function actionQuants() {
        $xml = file_get_contents('http://torebkihurt.pl/xml-porownywarki/hurtownia/index.php?hash=dropshipping-y1');
        $data = simplexml_load_string($xml);

        var_dump(count($data->produkty->produkt));

//        foreach ($data->produkty->produkt as $produkt) {
//            $quantity = (string) $produkt->liczba_egzemplarzy == '' ? null : (int) (string) $produkt->liczba_egzemplarzy;
//            $product_id = ((string) $produkt->attributes()->id);
//
//            $model = Product::findOne(['product_id' => $product_id]);
//
//            if (null != $model) {
//
//                $model->setAttributes([
//                    'quantity' => $quantity,
//                ]);
//
//                $model->save(false);
//            }
//        }
    }

    public function actionImport() {

        echo 'Start import';
        $xml = file_get_contents('http://torebkihurt.pl/xml-porownywarki/hurtownia/index.php?hash=dropshipping-y1');
        $data = simplexml_load_string($xml);

        $dataRet = [];
        $countProduct = 0;
        foreach ($data->produkty->produkt as $produkt) {

            $product_id = ((string) $produkt->attributes()->id);
            //if (isset($produkt->nazwa) && $produkt->nazwa != '' && !empty($product_id)) {
            $name = (string) $produkt->nazwa;
//echo $name;
            $price = (float) (string) $produkt->cena;
            $description = (string) $produkt->opis;
            $weight = (float) (string) $produkt->waga;
            $producer = (string) $produkt->producent;
            $vat = (int) $produkt->stawka_vat;
            $active = $produkt->wyswietlenie;

            if (isset($produkt->zdjecie_url)) {
                if (is_array($produkt->zdjecie_url)) {
                    $photoFirst = (string) $produkt->zdjecie_url[1];
                } else {
                    $photoFirst = (string) $produkt->zdjecie_url;
                }
            } else {
                $photoFirst = null;
            }
            $photoArray = $produkt->zdjecie_url;
            $category_id = (int) $produkt->kategoria_id;
            $quantity = (int) $produkt->liczba_egzemplarzy == '' ? 1 : $produkt->liczba_egzemplarzy;



            $modelType = 'stary';
            $model = Product::findOne(['product_id' => $product_id]);

            if (!$model) {
                echo 'Model nowy';
                $modelType = 'nowy';
                $model = new Product;
                $model->import_done = 0;
            } 


            if ($active == 1) { //&& $model->import_done == 0) {
                $parser = new Parser();
                $model->setAttributes([
                    'product_id' => $product_id,
                    'name' => $name,
                    'price_import' => $price,
                    'description' => $description,
                    'weight' => $weight,
                    'producer_id' => Producer::getDbId($producer),
                    'vat_percent' => $vat,
                    'active' => $active,
                    'quantity' => $quantity,
                    'import_category_id' => $category_id,
                ]);

                $model->save();
                $manager = new ImageManager(['driver' => 'gd']);

                if ($photoFirst) {
                    $path = $parser($photoFirst)['path'];
                    $file = $model->id . '_' . basename($path);
                    echo $file;
                    try {
                        $image = $manager->make($photoFirst);
                        $image->insert(Yii::getAlias('@app') . '/../frontend/web/images/watermark.png', 'bottom-right', 10, 10);
                        $image->save(Yii::getAlias('@app') . '/../frontend/web/products/' . $file, 90);
                        $model->photo = $file;
                        $model->save();
                    } catch (NotReadableException $e) {
                        echo $e->getMessage() . "\n";
                    }
                } else {
                    echo 'nie dodajemy zdjec';
                    $file = null;
                }

                $fileArray = [];
                $i = 0;
                foreach ($photoArray as $id => $photo) {
                    if ($i > 0) {
                        $file = $model->id . '_' . basename($parser($photo)['path']);
                        $fileArray[] = [$file];
                        try {
                            $image = $manager->make($photo);
                            $image->insert(Yii::getAlias('@app') . '/../frontend/web/images/watermark.png', 'bottom-right', 10, 10);
                            $image->save(Yii::getAlias('@app') . '/../frontend/web/products/' . $file, 90);

                            $imModel = new ShopProductImage();
                            $imModel->product_id = $model->id;
                            $imModel->photo = $file;
                            $imModel->position = 100;
                            $imModel->save();
                        } catch (NotReadableException $e) {
                            echo $e->getMessage() . "\n";
                        } finally {
                            $date = "First finally.\n";
                        }
                    }
                }
            }
            $i++;

            $model->import_done = 1;
            $model->save();
            if ($quantity > 0 && $active == 1) {
                $dataRet[] = [
                    //'wyswietlenie' => $produkt->wyswietlenie,
                    'model' => $model,
                    'name' => $name,
                    'quantity' => $quantity,
                    'active' => $active,
                    'type' => $modelType,
                    'date' => $date,
                    'file' => $fileArray,
                ];
                $countProduct++;
            }
        }

        return $this->render('import', [
            'countProduct' => $countProduct,
            'data' => $dataRet,
        ]);
    }

    public function actionClear() {
        $xml = file_get_contents('http://torebkihurt.pl/xml-porownywarki/hurtownia/index.php?hash=dropshipping-y1');
        $data = simplexml_load_string($xml);

        $dataRet = [];
        $countProduct = 0;
        foreach ($data->produkty->produkt as $produkt) {

            $product_id = ((string) $produkt->attributes()->id);
            $model = Product::findOne(['product_id' => $product_id]);
            if ($model) {
                $model->deleteAll();
                $shopImage = ShopProductImage::find()->where(['product_id' => $product_id])->delete();
            }
        }
    }

}

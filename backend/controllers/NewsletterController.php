<?php

namespace app\controllers;

use app\models\NewsletterEmail;
use app\models\Voucher;
use Yii;
use yii\base\Exception;
use yii\bootstrap\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class NewsletterController extends Controller
{
    public function actionSubscribe()
    {
        $model = new NewsletterEmail();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $link = $_SERVER['HTTP_HOST'] . '/newsletter/confirm/' . $model->mail . '/' . NewsletterEmail::hash($model->mail);

            Yii::$app->setting->mailer()->compose('subscribe', ['hash' => NewsletterEmail::hash($model->mail), 'link' => $link])->setTo($model->mail)->setFrom([Yii::$app->setting->get('smtp_login') => $_SERVER['HTTP_HOST']])->setSubject('Potwierdzenie otrzymywania newslettera w sklepie internetowym ' . Yii::$app->setting->get('app_name'))->send();

            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Na podany adres email zostało wysłane potwierdzenie'));
            return $this->redirect('/');

        }
        Yii::$app->getSession()->setFlash('error', Html::errorSummary($model));
        return $this->redirect('/');
    }

    public function actionConfirm($email, $hash)
    {

        try {

            if ($hash != NewsletterEmail::hash($email)) {
                throw new Exception(404, 'Nieprawidłowy adres');
            }

            $model = NewsletterEmail::findOne(['mail' => $email, 'active' => '0']);
            $model->active = 1;
            $model->save(false);

            $voucher = Voucher::generateNewsletterVoucher();
            Yii::$app->setting->mailer()->compose('nws-voucher', ['voucher' => $voucher])->setTo($model->mail)->setFrom([Yii::$app->setting->get('smtp_login') => $_SERVER['HTTP_HOST']])->setSubject('Kupon rabatowy na pierwsze zakupy ' . Yii::$app->setting->get('app_name'))->send();


            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Adres email został potwierdzony'));
        } catch (\Exception $e) {
            Yii::$app->getSession()->setFlash('error', 'Nieprawidłowy link');

        }
        return $this->redirect('/');

    }
}

<?php use yii\bootstrap\Html; ?>
<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Ostatnie zamówienia</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="table-responsive">
            <table class="table no-margin">
                <thead>
                <tr>
                    <th>Utworzono</th>
                    <th>Numer</th>
                    <th>Status</th>
                    <th>Klient</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($orders as $order): ?>
                    <tr>
                        <td><?= $order->date; ?></td>
                        <td>
                            <?= Html::a('#'.$order->__toString(), ['order/view', 'id' => $order->id]); ?>
                        </td>
                        <td><?= $order->statusName; ?></td>
                        <td><?= $order->client ? $order->client->__toString() : '?'; ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <!-- /.table-responsive -->
    </div>
    <!-- /.box-body -->
    <div class="box-footer clearfix">
        <a href="/order/index" class="btn btn-sm btn-default btn-flat pull-right">więcej</a>
    </div>
    <!-- /.box-footer -->
</div>
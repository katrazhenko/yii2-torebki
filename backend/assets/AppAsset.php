<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $sourcePath = '@vendor/';
    public $css = [
        '/css/site.css',
        'almasaeed2010/adminlte/dist/css/AdminLTE.css',
        'almasaeed2010/adminlte/dist/css/skins/skin-blue.min.css',
        'almasaeed2010/adminlte/plugins/morris/morris.css',
    ];
    public $js = [
        'almasaeed2010/adminlte/dist/js/app.js',
        'almasaeed2010/adminlte/plugins/morris/morris.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}

<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
	'homeUrl' => '/',
    'language' => 'pl',
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-backend',
			'baseUrl' => '',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'session' => [
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
		'urlManager' => [
				'enablePrettyUrl' => true,
                'showScriptName' => false,
		],
        'formatter' => [
            'class' => 'yii\\i18n\\Formatter',
            'nullDisplay' => '<span class="not-set">(not set)</span>',
            'dateFormat' => 'yyyy-dd-MM',
            'timeFormat' => 'medium',
            'datetimeFormat' => 'yyyy-dd-MM HH:mm',
            'booleanFormat' => [
                '<span class="glyphicon glyphicon-remove text-danger"></span>',
                '<span class="glyphicon glyphicon-ok text-success"></span>',
            ],
        ],
    ],

    'params' => $params,

    'modules' => [
        'yii2images' => [
            'class' => 'rico\yii2images\Module',
            'imagesStorePath' => '@frontend/web/images/store', //path to origin images
            'imagesCachePath' => '@frontend/web/images/cache', //path to resized copies
            'graphicsLibrary' => 'GD', //but really its better to use 'Imagick'
        //'imageDomain' => 'http://static.mysite.com/images/cache/',
        //   'placeHolderPath' => '@webroot/images/placeHolder.png', // if you want to get placeholder when image not exists, string will be processed by Yii::getAlias
        ],
        'cms' => [
            'class' => 'backend\modules\cms\Cms',
        ],
    ],
];

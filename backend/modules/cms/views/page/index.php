<?php

use leandrogehlen\treegrid\TreeGrid;
use backend\modules\cms\Cms;
use yii\helpers\Html;

$this->title = Cms::t('lbl', 'Pages');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading clearfix">
                <?= Html::a(Cms::t('btn', 'Create page'), ['create'], ['class' => 'btn btn-success pull-right btn-sm']) ?>
            </div>
            <div class="panel-body">
                <?= TreeGrid::widget([
                    'dataProvider' => $dataProvider,
                    'keyColumnName' => 'id',
                    'parentColumnName' => 'parent_id',
                    'parentRootValue' => 0,
                    'columns' => [
                        [
                            'header' => Cms::t('lbl', 'Type'),
                            'format' => 'raw',
                            'value' => function ($model) {
                                $out = Html::a('<i class="glyphicon glyphicon-arrow-up"></i>', ['page/position', 'id' => $model->id, 'position' => ($model->position - 3)]);
                                $out .= Html::a('<i class="glyphicon glyphicon-arrow-down"></i>', ['page/position', 'id' => $model->id, 'position' => ($model->position + 3)]);

                                return $out;
                            },
                        ],
                        'position',
                        'title',
                        [
                            'header' => Cms::t('lbl', 'Type'),
                            'attribute' => 'type_id',
                            'value' => function ($model) {
                                return $model->type;
                            },
                        ],
                        [
                            'header' => Cms::t('lbl', 'Template'),
                            'attribute' => 'template',
                            'value' => function ($model) {
                                return $model->getTemplate();
                            },
                        ],
                        [
                            'header' => Cms::t('lbl', 'Link'),
                            'attribute' => 'link',
                            'value' => function ($model) {
                                return $model->getLink();
                            },
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{update} {delete}',
                            'headerOptions' => ['width' => 70],
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>

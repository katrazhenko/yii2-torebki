<?php

use dosamigos\tinymce\TinyMceAsset;
use kartik\widgets\SwitchInput;
use backend\modules\cms\Cms;
use backend\modules\cms\models\I18n;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->registerJs(<<<'JS'
tinymce_settings = {
    selector: 'box-content',
    height: 500,
    width:'100%',
    statusbar: true,
    convert_urls: false,
    valid_children: '+body[style]',
    plugins: [
        "advlist autolink lists link charmap print preview anchor", 
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste image"
    ],
    toolbar: 'undo redo | styleselect | bold italic | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link code',
    content_css: [
        '/css/cms.css'
    ]};
    
    toggle_value=0;
    function editorToggle()
    {
        if(toggle_value) {
            var editor = tinymce.EditorManager.get('box-content').remove();
            toggle_value =0;
        } else {
            var editor = tinymce.EditorManager.createEditor('box-content',tinymce_settings).render();
            toggle_value =1;
        }
    }

JS
);
?>
<?php TinyMceAsset::register($this); ?>
<div class="boxes-form box-body">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'var')->textInput(['maxlength' => true]) ?>
    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'language')->dropDownList(I18n::getLanguages()) ?>
        </div>
        <div class="col-md-10">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true, 'autofocus' => true]) ?>
        </div>
    </div>

    <?= $form->field($model, 'content')->textarea(['rows' => 15]); ?>

    <label class="control-label"><?= Cms::t('lbl', 'WYSWIG Editor'); ?></label>
    <?= SwitchInput::widget([
        'name' => 'editor_switch',
        'value' => false,
        'id' => 'editor_switch',
        'pluginEvents' => ["switchChange.bootstrapSwitch" => "function() { editorToggle(); }"],
    ]); ?>

    <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'meta_title')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'meta_desc')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Cms::t('lbl', 'Create') : Cms::t('lbl', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

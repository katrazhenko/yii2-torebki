<?php

namespace backend\modules\cms;

use Yii;
use yii\base\Module;

class Cms extends Module
{

    public static function t($category, $message, $params = [], $language = null)
    {
        return Yii::t('modules/cms/'.$category, $message, $params, $language);
    }

    /**
     * @inheritdoc
     */
    public function init()
    {

        parent::init();
        $this->registerTranslations();
    }

    /**
     * Registers message bundle for this module
     */
    public function registerTranslations()
    {
        Yii::$app->i18n->translations['modules/cms/*'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'en',
            'basePath' => '@backend/modules/cms/messages',
            'fileMap' => [
                'modules/cms/btn' => 'button.php',
                'modules/cms/lbl' => 'label.php',
                'modules/cms/msg' => 'message.php',
            ],
        ];
    }
}

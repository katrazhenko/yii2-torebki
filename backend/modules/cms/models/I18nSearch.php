<?php

namespace backend\modules\cms\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * I18nSearch represents the model behind the search form about `app\modules\panel\models\I18n`.
 */
class I18nSearch extends I18n
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['var', 'language', 'value'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }


    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = I18n::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'var', $this->var])
            ->andFilterWhere(['like', 'language', Yii::$app->language])
            ->andFilterWhere(['like', 'value', $this->value]);

        return $dataProvider;
    }
}

<?php

namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;

use common\models\Category;
use common\models\LoginForm;
use common\models\Producer;
use common\models\Product;
use common\models\ShopProductImage;

use frontend\models\ContactForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ProductSearch;
use frontend\models\ResetPasswordForm;

use Intervention\Image\ImageManager;
use League\Uri\Parser;
use LukeSnowden\GoogleShoppingFeed\Containers\GoogleShopping;
use LukeSnowden\GoogleShoppingFeed\Item;

//use const YII_ENV_TEST;

/**
 * Site controller
 */
class SiteController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex() {
        Yii::$app->params['home'] = true;
        return $this->render('index');
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin() {

        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['shop/confirm']);
        }

        $model = new LoginForm();

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(['shop/confirm']);
        } else {
            return $this->render('login', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact() {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Dzięki za kontakt z nami. Zkontaktujemy się z tobą najszybciej jak to możliwe'));//'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', Yii::t('app', 'Wystąpił błąd przy wysyłce emaila.'));//There was an error sending email.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionRegulamin() {
        return $this->render('regulamin');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    /*   public function actionSignup()
      {
      $model = new SignupForm();
      if ($model->load(Yii::$app->request->post())) {
      if ($user = $model->signup()) {
      if (Yii::$app->getUser()->login($user)) {
      return $this->goHome();
      }
      }
      }

      return $this->render('signup', [
      'model' => $model,
      ]);
      } */

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset() {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
                    'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token) {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
                    'model' => $model,
        ]);
    }

    public function actionGoogleFeed() {

        GoogleShopping::title('Test Feed');
        GoogleShopping::link('https://www.slaskiecentrumkomputerowe.pl/');
        GoogleShopping::description('Our Google Shopping Feed');
        $searchModel = new ProductSearch();
        $ids = Category::getIdsWithSubcategories(1);
        $query = Product::find();
        $query->where('product_id is null and active = 1');
        $models = $query->all();


        foreach ($models as $model) {
            $item = GoogleShopping::createItem();
            $item->id('SCK_' . $model->id);
            $item->title($model->name);
            $desc = empty($model->description) ? $model->name : $model->description;
            $item->description(strip_tags($desc));
            $item->link(Url::to($model->url(), true));
            $item->image_link(Url::to($model->getUrl('250x250'), true));
            $item->condition(Item::USED);
            $item->availability(Item::INSTOCK);
            $item->price($model->price);
            $item->shipping('PL', 'Standard', "20.00");
        }

        GoogleShopping::asRss(true);
    }

    public function clean_string($string) {
        $s = trim($string);
        $s = iconv("UTF-8", "UTF-8//IGNORE", $s); // drop all non utf-8 characters
        // this is some bad utf-8 byte sequence that makes mysql complain - control and formatting i think
        $s = preg_replace('/(?>[\x00-\x1F]|\xC2[\x80-\x9F]|\xE2[\x80-\x8F]{2}|\xE2\x80[\xA4-\xA8]|\xE2\x81[\x9F-\xAF])/', ' ', $s);

        $s = preg_replace('/\s+/', ' ', $s); // reduce all multiple whitespace to a single space

        return $s;
    }

    public function actionImport() {
        $xml = file_get_contents('http://torebkihurt.pl/xml-porownywarki/hurtownia/kategorie.php?hash=dropshipping-y1');
        $data = simplexml_load_string($xml);
        foreach ($data as $cat) {
            echo '<pre>';
            var_dump($data->produkty->produkt[0]->nazwa);
            echo '</pre>';
            var_dump($cat->attributes()->id);
            var_dump($cat->attributes()->sciezka);
        }
    }

    public function actionTorebkiImport() {

        //$client = new Client();
        //$response = $client->get('http://torebkihurt.pl/xml-porownywarki/hurtownia/index.php?hash=dropshipping-y1');
        //$xml = $response->getBody()->getContents();
        //var_dump($xml);
        //exit();
        $xml = file_get_contents('http://torebkihurt.pl/xml-porownywarki/hurtownia/index.php?hash=dropshipping-y1');
        $data = simplexml_load_string($xml);


//         foreach ($data->produkty->produkt as $produkt) {
//            $nazwa = (string) $produkt->nazwa;
//            $cena = (float) (string) $produkt->cena;
//            $opis = (string) $produkt->opis;
//            $waga = (string) $produkt->waga;
//            $producent = (string) $produkt->producent;
//
//            $zdjecia = $produkt->zdjecie_url;
//
//            echo($opis.'<br>');
//        }
//        foreach ($data->produkty->produkt as $produkt) {
//            
//        }

        $product_id = ((string) $data->produkty->produkt[0]->attributes()->id);

        $name = (string) $data->produkty->produkt[0]->nazwa;
        $price = (float) (string) $data->produkty->produkt[0]->cena;
        $description = (string) $data->produkty->produkt[0]->opis;
        $weight = (float) (string) $data->produkty->produkt[0]->waga;
        $producer = (string) $data->produkty->produkt[0]->producent;
        $vat = (int) (string) $data->produkty->produkt[0]->stawka_vat;
        $active = (int) (string) $data->produkty->produkt[0]->wyswietlenie;
        $photo = isset($data->produkty->produkt[0]->zdjecie_url[0]) ? $data->produkty->produkt[0]->zdjecie_url[1] : null;
        $photos = $data->produkty->produkt[0]->zdjecie_url;
        $category_id = (int) (string) $data->produkty->produkt[0]->kategoria_id;


        $parser = new Parser();
        $manager = new ImageManager(['driver' => 'gd']);

        if (null != $photo) {
            $path = $parser($photo)['path'];
            $file = basename($path);
            $image = $manager->make($photo);
            $image->insert(Yii::getAlias('@webroot') . '/images/watermark.png', 'bottom-right', 10, 10);
            $image->save(Yii::getAlias('@webroot') . '/products/' . $file, 90);
        } else {
            $file = null;
        }
        $model = new Product;
        $producer_id = Producer::getDbId($producer);
        $model->setAttributes([
            'product_id' => $product_id,
            'name' => $name,
            'price_import' => $price,
            'description' => $description,
            'weight' => $weight,
            'producer_id' => $producer_id,
            'vat_percent' => $vat,
            'active' => $active,
            'photo' => $file,
            'quantity' => 1,
            'import_category_id' => $category_id,
        ]);

//        var_dump($vat);
//        exit(0);

        $model->save(false);

        $i = 0;
        foreach ($photos as $id => $photo) {

            if ($i > 0) {
                $path = $parser($photo)['path'];
                $file = basename($path);
                $image = $manager->make($photo);
                $image->insert(Yii::getAlias('@webroot') . '/images/watermark.png', 'bottom-right', 10, 10);
                $image->save(Yii::getAlias('@webroot') . '/products/' . $file, 90);

                $imModel = new ShopProductImage();
                $imModel->product_id = $model->id;
                $imModel->photo = $file;
                $imModel->position = 100;

                $imModel->save(false);
            }
            $i++;
        }



        //$zdjecia = $data->produkty->produkt[0]->zdjecie_url;
        //echo($nazwa);
        //$service = new Service();
        //$xml = \ForceUTF8\Encoding::fixUTF8($xml);
        //$result = $service->parse($xml);
        //$reader = new Reader();
        //$reader->xml(Encoding::fixUTF8($xml));
        //$result = $reader->parse();
        //var_dump($result);
    }

}

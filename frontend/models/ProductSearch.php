<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use common\models\{Product, Category, ShopProductAttribute};

/**
 * ProductSearch represents the model behind the search form about `common\models\Product`.
 */
class ProductSearch extends Product {

    /**
      * @var number price min
     */
    public $price_from;

    /**
     * @var float price max
     */
    public $price_to;

    /**
     * @var array list of attributes
     */
    public $attributes;
    
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'promotion', 'bargain', 'recommended', 'bestseller', 'new', 'quantity', 'producer', 'type', 'czas_dostawy', 'active', 'open', 'widget_offer', 'widget_recommend', 'widget_price', 'update_done', 'update_new'], 'integer'],
            [['product_id', 'part_number', 'name', 'description', 'warranty', 'photo', 'category_id', 'konfigurator', 'attributes'], 'safe'],
            [['price_from', 'price_to'], 'number'],
            [['price_import', 'price_basic', 'price', 'vat', 'mark'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public static function byPromotion($limit = 10) {
        return Product::find()->andWhere(['promotion' => 1])->andWhere(['>', 'quantity', '0'])->limit($limit)->all();
    }

    public static function byRecommend($limit = 10) {
        return Product::find()->andWhere(['recommended' => 1])->andWhere(['>', 'quantity', '0'])->limit($limit)->all();
    }

    /**
     * Creates data provider instance with search query applied
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params) {

        $query = Product::find()->where('quantity > 0');

        if (isset($_POST['sort'])) {
            Yii::$app->session->set('sort-list', $_POST['sort']);
        }

        if (isset($_POST['type'])) {
            Yii::$app->session->set('type-list', $_POST['type']);
        }


        $expression = new Expression('IF (quantity = 0,0,1) desc, shop_product.id desc');
        if (Yii::$app->session->has('sort-list')) {
            switch (Yii::$app->session->get('sort-list')) {
                case 'newest':
                    $query->orderBy($expression);
                    break;
                case 'oldest':
                    $expression = new Expression('IF (quantity = 0,0,1) desc, shop_product.id asc');
                    $query->orderBy($expression);
                    break;
                case 'priceA':
                    $expression = new Expression('IF (quantity = 0, 0, 1) desc, price asc');
                    $query->orderBy($expression);
                    break;
                case 'priceZ':
                    $expression = new Expression('IF (quantity = 0,0,1) desc, price desc');
                    $query->orderBy($expression);
                    break;

                case 'nameA':
                    $expression = new Expression('IF (quantity = 0, 0, 1) desc, name asc');
                    $query->orderBy($expression);
                    break;
                case 'nameZ':
                    $expression = new Expression('IF (quantity = 0,0,1) desc, name desc');
                    $query->orderBy($expression);
                    break;
            }
        } else {
            $query->orderBy($expression);
        }

        if (Yii::$app->session->has('type-list')) {
            switch (Yii::$app->session->get('type-list')) {
                case 'new':
                    $query->limit(1000);
                    $expression = new Expression('IF (quantity = 0,0,1) desc, shop_product.id desc');
                    $query->orderBy($expression);
                    break;
                case 'recommend':
                    $query->andFilterWhere(['recommended' => 1]);
                    break;
            }
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            return $dataProvider;
        }
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'price_import' => $this->price_import,
            'price_basic' => $this->price_basic,
            'price' => $this->price,
            'promotion' => $this->promotion,
            'bargain' => $this->bargain,
            'recommended' => $this->recommended,
            'bestseller' => $this->bestseller,
            'new' => $this->new,
            'quantity' => $this->quantity,
            'producer' => $this->producer,
            'czas_dostawy' => $this->czas_dostawy,
            'vat' => $this->vat,
            'active' => 1,
            'open' => $this->open,
            'mark' => $this->mark,
            'widget_offer' => $this->widget_offer,
            'widget_recommend' => $this->widget_recommend,
            'widget_price' => $this->widget_price,
            'update_done' => $this->update_done,
            'update_new' => $this->update_new,
        ]);


            $query->andFilterWhere(['>=', 'price', $this->price_from]);
            $query->andFilterWhere(['<=', 'price', $this->price_to]);

        if ($this->attributes) {

            $ids = [];
            $count = count($this->attributes);
            foreach($this->attributes as $k=>$v)
            {
                $sarray = [];
                foreach($v as $kk=>$vv) $sarray[] = $kk.'';

                $tmp = ArrayHelper::getColumn(ShopProductAttribute::find()->select('product_id')
                    ->andWhere(['in','value',$sarray])
                    ->andWhere(['attribute_id'=>$k])->all(),'product_id');

                if(!$ids)
                {
                    $ids = $tmp;
                }
                else
                {
                    $ids = array_intersect($ids,$tmp);
                }
            }

            $query->andFilterWhere(['in','id',$ids]);

            /*$query->joinWith(['productAttributes']);
            $arr = [];
            foreach ($this->attributes as $k => $v) {
                //$query->andWhere(['attribute_id' => $k, 'value' => key($v)]);

                $arr['attribute_id'][] = $k;
                //$query->andWhere(['attribute_id' => $k]);

                foreach ($v as $key => $val) {
                    //var_dump($key);
                    //$query->orWhere(['value' => $key]);
                    $arr['value'][] = $key;
                }
            }
            $query->andWhere($arr);*/
        }

        $query->andFilterWhere(['like', 'product_id', $this->product_id])
                ->andFilterWhere(['like', 'part_number', $this->part_number])
                ->andFilterWhere(['like', 'description', $this->description])
                ->andFilterWhere(['like', 'warranty', $this->warranty])
                ->andFilterWhere(['like', 'photo', $this->photo])
                ->andFilterWhere(['like', 'konfigurator', $this->konfigurator]);
        
        

        if (empty($this->name) === false) {
            $arr = explode(' ', $this->name);

            foreach ($arr as $key) {
                //$key = "%" . $key . '%';
                $query->andFilterWhere(['OR', ['like', 'product_id', $key], ['like', 'part_number', $key], ['like', 'name', $key]]);
            }
        }


        if (is_array($this->category_id)) {
            $query->andFilterWhere(['in', 'shop_product.category_id', $this->category_id]);
        } else {
            $query->andFilterWhere(['shop_product.category_id' => $this->category_id]);
        }

        return $dataProvider;
    }

    public function searchWord($params) {
        $query = Product::find();

        $expression = new Expression('IF (quantity = 0,0,1) desc, id desc');
        $query->orderBy($expression);

        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            $query->where('0=1');
            //var_dump($this->errors);
            return $dataProvider;
        }

        $arr = explode(' ', $this->name);

        foreach ($arr as $key) {
            
            $query->andFilterWhere(['OR', ['like', 'product_id', $key], ['like', 'used_product_id', $key],['like', 'part_number', $key], ['like', 'name', $key]]);
        }

        if ($this->category_id) {
            $ids = Category::getIdsWithSubcategories($this->category_id);

            //$query->andFilterWhere(['shop_product.category_id' => $ids]);
        }

        return $dataProvider;
    }

}

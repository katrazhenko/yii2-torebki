<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Product;

/**
 * ProductSearch represents the model behind the search form about `common\models\Product`.
 */
class ProductSearch extends Product
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'promotion', 'bargain', 'recommended', 'bestseller', 'new', 'quantity', 'producer', 'type', 'czas_dostawy', 'active', 'open', 'widget_offer', 'widget_recommend', 'widget_price', 'update_done', 'update_new'], 'integer'],
            [['product_id', 'part_number', 'name', 'description', 'warranty', 'photo', 'category_id', 'konfigurator'], 'safe'],
            [['price_import', 'price_basic', 'price', 'vat', 'mark'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public static function byPromotion($limit = 10)
    {
        return Product::find()->andWhere(['promotion' => 1])->andWhere(['>', 'quantity', '0'])->limit($limit)->all();
    }

    public static function byRecommend($limit = 10)
    {
        return Product::find()->andWhere(['recommended' => 1])->andWhere(['>', 'quantity', '0'])->limit($limit)->all();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Product::find()->where('quantity>0');
        
        if(isset($_POST['sort']))
        {
            Yii::$app->session->set('sort-list',$_POST['sort']);
        }

        if(Yii::$app->session->has('sort-list'))
        {
            switch(Yii::$app->session->get('sort-list'))
            {
                case 'newest': $query->orderBy('id desc'); break;
                case 'priceA': $query->orderBy('price asc'); break;
                case 'priceZ': $query->orderBy('price desc'); break;
            }
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            $query->andWhere('0=1');
            return $dataProvider;
        }
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'price_import' => $this->price_import,
            'price_basic' => $this->price_basic,
            'price' => $this->price,
            'promotion' => $this->promotion,
            'bargain' => $this->bargain,
            'recommended' => $this->recommended,
            'bestseller' => $this->bestseller,
            'new' => $this->new,
            'quantity' => $this->quantity,
            'producer' => $this->producer,
            'czas_dostawy' => $this->czas_dostawy,
            'vat' => $this->vat,
            'active' => $this->active,
            'open' => $this->open,
            'mark' => $this->mark,
            'widget_offer' => $this->widget_offer,
            'widget_recommend' => $this->widget_recommend,
            'widget_price' => $this->widget_price,
            'update_done' => $this->update_done,
            'update_new' => $this->update_new,
        ]);

        if($this->type==='1')
        {
            $query->andWhere(['is', 'product_id', null]);
        }

        if($this->type==='0')
        {
            $query->andWhere(['is not', 'product_id', null]);
        }

        $query->andFilterWhere(['like', 'product_id', $this->product_id])
            ->andFilterWhere(['like', 'part_number', $this->part_number])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'warranty', $this->warranty])
            ->andFilterWhere(['like', 'photo', $this->photo])
            ->andFilterWhere(['like', 'konfigurator', $this->konfigurator]);

        if (is_array($this->category_id)) {
            $query->andFilterWhere(['in', 'category_id', $this->category_id]);
        } else {
            $query->andFilterWhere(['category_id' => $this->category_id]);
        }

        return $dataProvider;
    }
}

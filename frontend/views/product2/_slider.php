<div class="product-page-box text-center slider-product">
    <div class="padding-product" onclick="window.location.href = '<?= $model->url(); ?>'">
        <?= $model->img(['class' => 'height-limit']); ?>
        <div class="row product-caption">
            <div class="col-md-6">
                <div class="category">
                    <a class="title" href="<?= $model->url(); ?>">
                        <?= mb_substr($model->name, 0, 28) . (mb_strlen($model->name) > 28 ? '...' : ''); ?>
                    </a>
                    <?= $model->getCategoryName(); ?>
                </div>
            </div>
            <div class="col-md-6">
                <p class="product-price">
                    <?php if ($model->isPromo()): ?>
                        <span><?= $model->basicPriceBrutto(); ?></span>
                    <?php endif; ?>
                    <?= $model->priceBrutto(); ?>
                </p>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
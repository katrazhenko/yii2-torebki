<?php 
use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <a href=" <?= Url::home() ?>"><?= Html::img('/images/logo.png', ['class' => 'img-responsive logo']); ?></a>
        </div>

        <div class="col-md-5">
            <?= $this->render('_search'); ?>
        </div>

        <div class="col-md-4">
            <?= $this->render('_nav_icon', [ 'model' => $model, ]); ?>
        </div>
    </div>
</div>

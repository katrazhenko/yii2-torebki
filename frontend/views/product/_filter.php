<?php 
    use yii\helpers\Html;
?>
  <li style="border-top: 1px solid silver; box-shadow: 0px -4px 4px #e6e6e6; margin-top: 10px;">
      <h4>Filtrowanie</h4>
      <hr class="sidebar-header-delimiter"/>
  </li>
  <li class="remove-filters" style="padding: 0px 20px 0px 20px; text-align: right"> </li>

  <div>
      <?php foreach ($filters as $filter_key => $filter): ?>
          <div class="system-filter">
              <li class="filter-name"><?= $filter[0]['name']; ?></li>
              <li class="filter-content">

        <?php foreach ($filter as $option_key => $option){
                $value = $option['value'];
                echo Html::checkbox("ProductSearch[attributes][$filter_key][$value]") . ' ' . $value; ?>
                <br/>
        <?php }; ?>


              </li>
          </div>
      <?php endforeach; ?>
  </div>

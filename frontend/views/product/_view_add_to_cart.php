<input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>"/>

<div class="form-group">
    <input type="text" name="q" value="1" class="form-control">
</div>

<button type="submit" class="btn btn-default">Do koszyka</button>

<br>
<br>
Dostępność produktu: <span class="text-warning">tylko <?= $model->quantity; ?> sztuk</span>
<div class="clear20"></div>

<?php if ($model->getKonfigurator() and !$model->product_id){ ?>
    <?= $this->render('_view_configurator_pc', [ 'model' => $model, ]); ?>
    <?php $this->registerJs('
    var price = 0;
    $(".options").change(function(){
        price=0;
        $(".options :selected").each(function(){ price += $(this).data("price"); });
        $(".mainprice").html( ( $(".mainprice").data("price")*1 + price*1).toFixed(2) +" <span>PLN</span>");
    });
    '); ?>
<?php } ?>

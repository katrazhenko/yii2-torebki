      <a href="<?= $model->url(); ?>" class="img-block" data-pjax="0"><?= $model->img(); ?></a>
      <a class="title" href="<?= $model->url(); ?>" data-pjax="0">
          <?= $model->name; ?>
      </a>

      <p class="product-price">
          <?php if($model->isPromo()): ?>
              <span><?= $model->basicPriceBrutto(); ?></span>
          <?php endif; ?>
          <?= $model->priceBrutto(); ?>
      </p>

      <div class="category">
          <?= $model->categoryName; ?>
      </div>
  <a class="button" href="/shop/add?id=<?= $model->id; ?>" data-pjax="0">Do koszyka</a>

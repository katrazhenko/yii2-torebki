<?php

use common\models\Client;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Client */

$this->title = Yii::t('app', 'Załóż konto');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Clients'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <?php if($model->scenario == Client::SCENARIO_REGISTER):?>
    <h2 class="beauty"><span>Zarejestruj się</span></h2>
    <?php else: ?>
        <h2 class="beauty"><span>Zamówienie bez rejestracji</span></h2>
    <?php endif; ?>
    <div class="clear20"></div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
    <div class="clear20"></div>
</div>




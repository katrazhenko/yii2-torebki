<?php

use frontend\models\ProductSearch;
use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="container">
    <!-- cart -->
    <div class="cart-panel">
        <div class="title">Twój koszyk</div>
        <table class="table">
            <thead>
                <tr>
                    <th>Produkt</th>
                    <th></th>
                    <th>Cena</th>
                    <th>Ilość</th>
                    <th>Razem</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($basket->getItems() as $k => $item): ?>
                    <tr>
                        <th scope="row"><?= $item['product']->img(['width' => 64]); ?></th>
                        <td>
                            <a href="<?= $item['product']->url(); ?>"><?= $item['product']->name; ?></a>
                            <div class="product-category"><?= $item['product']->category->title; ?></div>

                            <?php if ($item['attributes']['options']): ?>
                                <?php foreach ($item['attributes']['options'] as $option): ?>
                                    <?= $option['name']; ?> (+ <?= $option['price']; ?> zł)
                                <?php endforeach; ?>
                            <?php endif; ?>

                        </td>
                        <td><?= $item['product']->priceBrutto() + $item['add_price']; ?> PLN</td>
                        <td>
                            <input type="text" class="form-control quantity" value="<?= $item['attributes']['quantity']; ?>" data-key="<?= $k; ?>"/>
                        </td>
                        <td><b><?= ($item['product']->priceBrutto() + $item['add_price']) * $item['attributes']['quantity']; ?> PLN</b></td>
                        <td><a href="/shop/delete?key=<?= $k; ?>"><span class="glyphicon glyphicon-trash" style="color: gray;"></span></a></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>


        <div class="row">
            <div class="col-md-5"><div class="nheader">Wybierz sposób dostawy lub odbioru</div></div>
            <div class="col-md-5"><div class="nheader">Wybierz sposób płatności</div></div>
        </div>
        <div class="gray-box">
            <div class="row">
                <div class="col-md-5 delivery">
                    <div class="images">
                        <?php
                        
                        if (isset($deliveryModel) && !empty($deliveryModel)) {
                            foreach ($deliveryModel as $delivery) {
                                ?>
                                <a href="<?= Url::to(['/shop/transport', 'id'=>$delivery->id])?>">
                                    <div class="image-container<?= $basket->getTransportId() == $delivery->id ? ' active' : ''; ?>">
                                        <img src="/img/kurier.png" width="80px" class="img-responsive">
                                    </div>
                                    <div class="caption"><?= $delivery->name; ?></div>
                                </a>
                                <?php
                            }
                        }
                        ?>

                        <?php
                        /*
                          <a href="/shop/transport?id=10">
                          <div class="image-container<?= $basket->getTransportId() == 10 ? ' active' : ''; ?>">
                          <img src="/img/odbior_osobisty.png" width="80px" class="img-responsive">
                          </div>
                          <div class="caption">Odbiór osobisty Gliwice</div>
                          </a>
                          <a href="/shop/transport?id=11">
                          <div class="image-container<?= $basket->getTransportId() == 11 ? ' active' : ''; ?>">
                          <img src="/img/odbior_osobisty.png" width="80px" class="img-responsive">
                          </div>
                          <div class="caption">Odbiór osobisty Białystok</div>
                          </a>
                          <a href="/shop/transport?id=6">
                          <div class="image-container<?= $basket->getTransportId() == 6 ? ' active' : ''; ?>">
                          <img src="/img/kurier_pobranie.png" width="80px" class="img-responsive">
                          </div>
                          <div class="caption">Kurier DPD za pobraniem</div>
                          </a>
                         * 
                         */
                        ?>
                    </div>
                </div>
                <div class="col-md-5"  style="border-left:10px solid #fff">
                    <div class="images">

                        <?php if (in_array('przelew', $payments)): ?>
                            <a href="/shop/payment?id=przelew">
                                <div class="image-container<?= $basket->getPaymentId() == 'przelew' ? ' active' : ''; ?>">
                                    <img src="/img/przelew.png" width="80px" class="img-responsive">
                                </div>
                                <div class="caption">Przelew</div>
                            </a>
                        <?php endif; ?>
                        <?php if (in_array('online', $payments)): ?>
                            <a href="/shop/payment?id=online">
                                <div class="image-container<?= $basket->getPaymentId() == 'online' ? ' active' : ''; ?>">
                                    <img src="/img/credit_card.png" width="80px" class="img-responsive">
                                </div>
                                <div class="caption">Płatność online</div>
                            </a> 
                        <?php endif; ?>

                        <?php if (in_array('odbior', $payments)): ?>
                            <a href="/shop/payment?id=odbior">
                                <div class="image-container<?= $basket->getPaymentId() == 'odbior' ? ' active' : ''; ?>">
                                    <img src="/img/transfer.png" width="80px" class="img-responsive">
                                </div>
                                <div class="caption">Płatność przy odbiorze</div>
                            </a> 
                        <?php endif; ?>
                        <?php if (in_array('raty', $payments)): ?>
                            <a href="/shop/payment?id=raty">
                                <div class="image-container<?= $basket->getPaymentId() == 'raty' ? ' active' : ''; ?>">
                                    <img src="/img/zagiel.png" width="80px" class="img-responsive">
                                </div>
                                <div class="caption">Raty Żagiel</div>
                            </a>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="col-md-2">
                    Koszt wysyłki
                    <div class="delivery-price"><?= $basket->getTransportCost(); ?> zł</div>
                </div>
            </div>
        </div>

        <form action="/shop/confirm" method="post" id="confirm-form">
            <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>"/>
            <div class="nheader">Podsumowanie</div>
            <div class="row">
                <div class="col-md-7">
                    <div class="gray-box">
                        <b>Twoje uwagi do zamówienia</b>
                        <input type="text" name="notice" value="<?= $basket->getNotice(); ?>" placeholder="Wpisz tutaj uwagi do twojego zamówienia" class="form-control comments">
                    </div>
                </div>

                <div class="col-md-5">
                    <div class="gray-box">
                        <b>Kwota do zapłaty:</b>
                        <span class="sum-price"><?= $basket->toPay(); ?> zł</span><br><br>
                        <?php if ($basket->productsPrice() < 1000): ?>
                            <div class="row">
                                <div class="col-md-2">
                                    <img src="/img/warning_icon.png" width="32" class="img-responsive"/>
                                </div>
                                <div class="col-md-8">
                                    <span style="color: #f14837; font-size: 13px;"><b>Dokonaj zakupu na kwote 1000,00 zł brutto a otrzymasz przesyłkę gratis!</b></span>
                                </div>
                            </div>
                        <?php endif ?>
                    </div>
                </div>
            </div>
            <div class="clear50"></div>
            <div class="row">
                <div class="col-md-3">
                    <a href="/" class="btn btn-default">Kontynuuj zakupy</a>
                </div>
                <div class="col-md-4 text-right">
                    <div class="form-inline ">
                        <label>
                            Kod rabatowy: <input type="text" class="form-control" name="voucher-id" placeholder="Wpisz kod rabatowy" style="width:155px" />
                            <button type="submit" name="voucher" class="btn btn-default">Zastosuj</button>
                        </label>
                    </div>
                </div>
                <div class="col-md-5 text-right">

                    <button type="submit" class="btn btn-primary">Zamówienie z obowiązkiem zapłaty</button>
                </div>
            </div>

        </form>
        <div class="clear50"></div>
        <div class="accessories">
            <div class="title">
                <h4 class="beauty">
                    Ponadto polecamy
                </h4>
            </div>



            <div class="product-list">
                <div class="container-fluid">
                    <div class="row">
                        <?php if ($list = ProductSearch::byRecommend(4)): ?>
                            <?php foreach ($list as $product): ?>
                                <div class="col-md-6 col-lg-3">
                                    <a href="<?= $product->url(); ?>">
                                        <?= $product->img(['style' => "width: 128px; height: 128px;", 'class' => "img-responsive"]); ?>
                                        <b>
                                            <?= mb_substr($product->name, 0, 28) . (mb_strlen($product->name) > 28 ? '...' : ''); ?>
                                        </b>
                                        <div class="price">
                                            <?php if ($product->isPromo()): ?>
                                                <span><?= $product->basicPriceBrutto(); ?></span>
                                            <?php endif; ?>
                                            <?= $product->priceBrutto(); ?> zł</div>
                                        <span style="color: gray; font-size: 11px;"><?= $product->category->title; ?></span>
                                    </a>
                                </div>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
$this->registerCss('.alert-danger{display:none; margin-top: 17px;}');
?>

<?php
$this->registerJS('
    $(".quantity").change(function(){
        window.location.href="/shop/change?key="+$(this).data("key")+"&quantity="+$(this).val();
    });    
    $(".alert-danger").detach().prependTo("#confirm-form").show();

');

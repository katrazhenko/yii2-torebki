<?php

namespace frontend\modules\cms\models;

use frontend\modules\cms\Cms;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%i18n}}".
 *
 * @property string $var
 * @property string $lang
 * @property string $value
 */
class I18n extends ActiveRecord
{
    static $_tmp;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cms_i18n}}';
    }

    public static function getLanguages()
    {
        return [
            'pl' => 'Polski',
        ];
    }

    public static function g($string, $string1, $string2)
    {
        if(!self::$_tmp) {
            self::$_tmp = ArrayHelper::map(I18n::find()->all(),'var','value');
        }
        return self::$_tmp[$string] ?? $string2 ?? $string ?? '';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['var', 'language', 'value'], 'required'],
            [['var'], 'string', 'max' => 20],
            [['lang'], 'string', 'max' => 2],
            [['value'], 'string', 'max' => 512],
            [['var', 'language'], 'unique', 'targetAttribute' => ['var', 'language'], 'message' => 'The combination of Var and Lang has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'var' => Cms::t('app', 'Var'),
            'language' => Cms::t('app', 'Language'),
            'value' => Cms::t('app', 'Value'),
        ];
    }


}

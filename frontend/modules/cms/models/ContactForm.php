<?php

namespace frontend\modules\cms\models;

use frontend\modules\cms\models\I18n;
use Yii;
use yii\base\Model;
/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $email;
	public $phone;
	
    public $company;
	public $city;
	public $street;
	public $imageFile;
	
    public $body;
    public $verifyCode;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'phone', 'body'], 'required'],
            [['email', 'company','city','street'],'string'],
            // email has to be a valid email address
            ['email', 'email'],
           // [['imageFile'], 'file', 'skipOnEmpty' => false],
            // verifyCode needs to be entered correctly
      //      ['verifyCode', 'captcha'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'name'=>I18n::g('name','form','Imię i nazwisko'),
            'phone'=>I18n::g('phone','form','Telefon'),
            'email'=>I18n::g('email','form','Email'),
            'company'=>I18n::g('company','form','Company name'),
            'city'=>I18n::g('city','form','City'),
            'body'=>I18n::g('body','form','Treść'),
            'street'=>'Ulica, numer',
            'imageFile'=>'Załącznik',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param  string  $email the target email address
     * @return boolean whether the model passes validation
     */
    public function contact()
    {
        if ($this->validate()) {
            Yii::$app->mailer->compose('contact-html',['fields'=>$this->attributeLabels()])
                        ->setTo($this->email)
                        ->setFrom([Yii::$app->params['adminEmail'] => $_SERVER['HTTP_HOST']])
                        //->setBcc(Yii::$app->params['adminEmail'])
                        ->setSubject('Formularz kontaktowy')
                        ->send();
            
//            Yii::$app->setting->mailer()->compose( 'contact-html',['fields'=>$this->attributeLabels()])
//                ->setTo($this->email)
//                ->setFrom([ Yii::$app->setting->get('smtp_login') => $_SERVER['HTTP_HOST']])
//                ->setSubject('Formularz kontaktowy')
//                    ->send();
			
            return true;
        }
        return false;
    }
}

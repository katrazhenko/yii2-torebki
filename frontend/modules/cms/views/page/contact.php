<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use frontend\modules\cms\models\I18n;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Contact';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pages-default-index">
    <section class="hgroup">
        <div class="container">
            <h1><?= $model->title ?></h1>
        </div>
    </section>
    <section class="article-text">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <?= $model->content; ?>
                </div>
                <div class="col-md-6">

                    <h4>Formularz kontaktowy</h4>
                    <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>
                        <div class="alert alert-success">
                            <?= I18n::g('after_send', 'form', 'Wiadomość została wysłana. Dziękujemy.') ?>
                        </div>
                    <?php else: ?>

                        <?php $form = ActiveForm::begin(['id' => 'contact-form', 'options' => ['enctype' => 'multipart/form-data']]); ?>
                        <div class="row">
                            <div class="col-sm-12">
                                <?= $form->field($cmodel, 'name')->textInput(['placeholder' => I18n::g('name', 'form', 'Imię i nazwisko')])->label(false) ?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <?= $form->field($cmodel, 'phone')->textInput(['placeholder' => I18n::g('phone', 'form', 'Telefon')])->label(false) ?>
                            </div>
                            <div class="col-sm-6">
                                <?= $form->field($cmodel, 'email')->textInput(['placeholder' => I18n::g('email', 'form', 'Email')])->label(false) ?>
                            </div>
                        </div>
                        <?= $form->field($cmodel, 'body')->textArea(['rows' => 10, 'placeholder' => I18n::g('body', 'form', 'Treść')])->label(false) ?>
                        <div class="form-group">
                            <?= Html::submitButton(I18n::g('submit', 'form', 'Wyślij'), ['class' => 'btn btn-primary pull-right', 'name' => 'contact-button']) ?>
                        </div>
                        <div class="clearfix"></div>
                        <?php ActiveForm::end(); ?><?php endif; ?>
                </div>
            </div>
        </div>
    </section>

</div>

<div class="container">
    <div class="clear20"></div>
    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d20391.74965839308!2d18.66902!3d50.292512!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x471130f9318de41d%3A0x32a2d6444ce8c9b5!2sDworcowa+20%2C+44-100+Gliwice!5e0!3m2!1spl!2spl!4v1477211239482" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
    <div class="clear20"></div>
</div>

<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Category;

/**
 * CategorySearch represents the model behind the search form about `common\models\Category`.
 */
class CategorySearch extends Category {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'import_id', 'parent', 'position', 'active'], 'integer'],
            [['title', 'filters', 'description'], 'safe'],
            [['margin'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Category::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'import_id' => $this->import_id,
            'parent' => $this->parent,
            'position' => $this->position,
            'active' => $this->active,
            'margin' => $this->margin,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
                ->andFilterWhere(['like', 'filters', $this->filters])
                ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }

    public function searchNoPages($params) {
        $query = Category::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

//        $this->load($params);
//
//        if (!$this->validate()) {
//            // uncomment the following line if you do not want to return any records when validation fails
//            // $query->where('0=1');
//            return $dataProvider;
//        }
//
//        // grid filtering conditions
//        $query->andFilterWhere([
//            'id' => $this->id,
//            'import_id' => $this->import_id,
//            'parent' => $this->parent,
//            'position' => $this->position,
//            'active' => $this->active,
//            'margin' => $this->margin,
//        ]);
//
//        $query->andFilterWhere(['like', 'title', $this->title])
//                ->andFilterWhere(['like', 'filters', $this->filters])
//                ->andFilterWhere(['like', 'description', $this->description]);

        $dataProvider->pagination = false;

        return $dataProvider;
    }

}

<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "shop_producer".
 *
 * @property string $id
 * @property string $producer_id
 * @property string $name
 */
class Producer extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'shop_producer';
    }

    /**
     * __toString()
     *
     * @return string
     */
    public function __toString() {
        return $this->name;
    }

    public static function getFilterList() {
        return ArrayHelper::map(self::find()->asArray()->all(), 'id', 'name');
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['producer_id', 'name'], 'required'],
            [['producer_id'], 'string', 'max' => 50],
            [['name'], 'string', 'max' => 100],
            [['producer_id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'producer_id' => Yii::t('app', 'ID Producenta'),
            'name' => Yii::t('app', 'Nazwa'),
        ];
    }

    public static function getDbId(string $producer) {
        if (empty($producer) === false) {
            $model = Producer::findOne(['name' => $producer]);
            if ($model == null) {
                $model = new Producer();
                $model->name = $producer;
                $model->save(false);
            }
            return $model->id;
        } else {
            return null;
        }
    }

}

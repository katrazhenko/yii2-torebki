<?php

namespace common\models;

use dosamigos\transliterator\TransliteratorHelper;
use Imagine\Image\Box;
use Yii;
use yii\bootstrap\Html;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Inflector;
use yii\imagine\Image;

/**
 * This is the model class for table "shop_product".
 *
 * @property string $id
 * @property string $product_id
 * @property string $part_number
 * @property integer $category_id
 * @property string $name
 * @property string $description
 * @property double $price_import
 * @property double $price_basic
 * @property double $price
 * @property integer $promotion
 * @property integer $bargain
 * @property integer $recommended
 * @property integer $bestseller
 * @property integer $new
 * @property integer $quantity
 * @property integer $producer_id
 * @property string $warranty
 * @property integer $type
 * @property integer $czas_dostawy
 * @property double $vat
 * @property string $photo
 * @property integer $active
 * @property string $open
 * @property double $mark
 * @property string $konfigurator
 * @property integer $widget_offer
 * @property integer $widget_recommend
 * @property integer $widget_price
 * @property integer $update_done
 * @property integer $update_new
 */
class Product extends ActiveRecord {

    public $folderTmp = false, $image, $folder;

    const VAT = 1.23;
    const TYPE_NEW = 1;
    const TYPE_USED = 2;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'shop_product';
    }

    public static function priceWithMargin($price, $percent) {
        return sprintf("%.2f", $price * (1 + $percent / 100));
    }

    public static function promoMenu() {
        return self::find()->andWhere(['active' => 1, 'bestseller' => 1])->andWhere(['!=', 'photo', ''])->limit(1)->one();
    }

    /**
     * __toString()
     *
     * @return string
     */
    public function __toString() {
        return $this->name;
    }

    public static function getFilterList() {
        return ArrayHelper::map(self::find()->asArray()->all(), 'id', 'name');
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['category_id', 'name', 'price', 'quantity'], 'required'],
            [['product_id', 'category_id', 'promotion', 'bargain', 'recommended', 'bestseller', 'new', 'quantity', 'producer_id', 'czas_dostawy', 'active', 'widget_offer', 'widget_recommend', 'widget_price', 'update_done', 'update_new'], 'integer'],
            [['description', 'folder'], 'string'],
            [['price_import', 'price_basic', 'price', 'vat', 'vat_percent', 'mark'], 'number'],
            [['part_number'], 'string', 'max' => 200],
            [['image', 'konfigurator', 'import_category_id'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['warranty'], 'string', 'max' => 4],
            [['photo'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'category_name' => 'Nazwa kategorii',
            'id' => Yii::t('app', 'ID'),
            'product_id' => Yii::t('app', 'Produkt'),
            'part_number' => Yii::t('app', 'Numer producenta'),
            'category_id' => Yii::t('app', 'Kategoria'),
            'name' => Yii::t('app', 'Nazwa'),
            'description' => Yii::t('app', 'Opis'),
            'price_import' => Yii::t('app', 'Cena zakupu brutto'),
            'price_basic' => Yii::t('app', 'Cena sugerowana brutto'),
            'price' => Yii::t('app', 'Cena sprzedaży brutto'),
            'promotion' => Yii::t('app', 'Slider Główna (slider mały)'),
            'bargain' => Yii::t('app', 'Okazja'),
            'recommended' => Yii::t('app', 'Polecane (strona główna)'),
            'bestseller' => Yii::t('app', 'Gorący strzał'),
            'new' => Yii::t('app', 'Nowość'),
            'quantity' => Yii::t('app', 'Ilość'),
            'producer_id' => Yii::t('app', 'Producent'),
            'warranty' => Yii::t('app', 'Gwarancja'),
            'type' => Yii::t('app', 'Produkt poleasingowy'),
            'czas_dostawy' => Yii::t('app', 'Czas Dostawy'),
            'vat' => Yii::t('app', 'Vat'),
            'photo' => Yii::t('app', 'Photo'),
            'active' => Yii::t('app', 'Aktywna'),
            'open' => Yii::t('app', 'Open'),
            'mark' => Yii::t('app', 'Mark'),
            'konfigurator' => Yii::t('app', 'Konfigurator'),
            'widget_offer' => Yii::t('app', 'Widget Offer'),
            'widget_recommend' => Yii::t('app', 'Widget Recommend'),
            'widget_price' => Yii::t('app', 'Widget Price'),
            'update_done' => Yii::t('app', 'Update Done'),
            'update_new' => Yii::t('app', 'Update New'),
        ];
    }

    public function deleteImages() {
        if (isset($_POST['deleteImage'])) {

            foreach ($_POST['deleteImage'] as $key => $value) {
                $image = ShopProductImage::findOne($key);
                $image->deleteFiles();
                $image->delete();
            }
        }
    }

    public function addImages() {
        $path = Yii::getAlias('@backend/web/tmp') . DIRECTORY_SEPARATOR . $this->folder;

        if (is_dir($path)) {
            $files = FileHelper::findFiles(Yii::getAlias('@backend/web/tmp') . DIRECTORY_SEPARATOR . $this->folder);

            foreach ($files as $file) {
                $path = Yii::getAlias('@frontend/web/products') . DIRECTORY_SEPARATOR . basename($file);
                rename($file, $path);
                $image = new ShopProductImage();
                $image->product_id = $this->id;
                $image->photo = basename($file);
                $image->save(false);
            }
        }
    }

    public function getUrl($size) {
        $whSize = explode('x', $size);
        $path = Yii::getAlias('@frontend/web/products') . DIRECTORY_SEPARATOR;
        if (!file_exists($path . $size . '___' . $this->photo)) {

            if (!file_exists($path . $this->photo) or ! $this->photo) {
                return '/logo.jpg';
            }

            Image::getImagine()->open($path . $this->photo)->thumbnail(new Box($whSize[0], $whSize[1]))->save($path . $size . '___' . $this->photo, ['quality' => 90]);
            //Image::thumbnail($path . $this->photo, $whSize[0], $whSize[1])->save($path . $size . '___' . $this->photo, ['quality' => 90]);
        }
        return '/products/' . $size . '___' . $this->photo;
    }

    public function breadcrumbs() {
        $categories = [];

        $tmp = $this->getCategory()->one();
        if (null != $tmp) {
            $categories[] = $tmp;
            $i = 0;
            if ($tmp) {
                while ($tmp->getParentCategory()) {

                    $i++;
                    $categories[] = $tmp = $tmp->getParentCategory();
                    if ($i > 10)
                        break;
                }
            }

            $categories = array_reverse($categories);
        }

        return $categories;
    }

    public function getCurrentBreadcrumbTitle() {
        $breads = $this->breadcrumbs();
        if (empty($breads) === false && isset(current($breads)->title)) {
            return current($breads)->title;
        } else {
            return null;
        }
    }

    public function getKonfigurator() {
        if (is_array($this->konfigurator)) {
            return $this->konfigurator;
        }
        if ($this->konfigurator) {
            return $this->konfigurator = unserialize($this->konfigurator);
        } else {
            return $this->konfigurator = [];
        }
    }

    public function konfiguratorObjects() {
        $r = [];
        if ($this->getKonfigurator()) {

            if (is_array($this->konfigurator))
                foreach ($this->konfigurator as $k => $v)
                    if ($tmp = ShopConfigurator::findOne($v)) {
                        $r[$tmp->type][] = $tmp;
                    }
        }

        return $r;
    }

    public function beforeSave($insert) {
        $this->konfigurator = serialize($this->konfigurator);

        if (isset($_POST['mainImage'])) {
            $image = ShopProductImage::findOne($_POST['mainImage']);
            $this->photo = $image->photo;
        }

        $this->setSellPrice();
        $this->setCategory();

        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes) {

        $this->deleteImages();
        $this->addImages();
        parent::afterSave($insert, $changedAttributes);
    }

    public function setCategory() {

        if (empty($this->import_category_id) === false && empty($this->category_id) === true) {
            $cat = Category::findOne(['import_id' => $this->import_category_id]);
            if (null != $cat) {
                $this->category_id = $cat->id;
            }            
        }
    }

    public function setSellPrice() {

        if (empty($this->price_import) === false && empty($this->price) === true) {
            $defmargin = (int) Yii::$app->setting->get('default_margin');
            $defvat = empty($this->vat_percent) === false ? (int) $this->vat_percent : (int) Yii::$app->setting->get('default_vat');
            $this->price = $this->price_import + ($this->price_import * ($defmargin / 100));
            $this->price = $this->price + ($this->price * ($defvat / 100));
        }
    }

    public function getImageFolderName() {
        if (!$this->folderTmp) {
            $this->folderTmp = uniqid('p_');
        }
        return $this->folderTmp;
    }

    public function getCategory() {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    public function img($options = []) {
        if ($this->photo) {
            return Html::img($this->getUrl('250x250'), $options);
        } else {
            return Html::img('/logo.jpg', $options);
        }
    }

    public function getMargin() {
        $tmp = $this->getCategory()->one();

        if ($tmp && $tmp->margin > 0) {
            return $tmp->margin;
        }

        $i = 0;
        if ($tmp) {
            while ($tmp->getParentCategory()) {

                if ($tmp->margin > 0) {
                    return $tmp->margin;
                }

                $i++;
                $categories[] = $tmp = $tmp->getParentCategory();



                if ($i > 10)
                    break;
            }
        }

        return Yii::$app->setting->get('default_margin');
    }

    public function getCategoryName() {
        if (null != $this->category) {
            return $this->category->title;
        } else {
            return null;
        }
    }

    public function priceBrutto() {
        $margin = $this->getMargin() / 100;
        return sprintf("%.2f", ($this->price + $this->price * $margin)
        );
    }

    public function priceNetto() {
        $margin = $this->getMargin() / 100;
        return sprintf("%.2f", $this->priceBrutto() / static::VAT
        );
    }

    public function basicPriceBrutto() {
        $margin = $this->getMargin() / 100;
        return sprintf("%.2f", ($this->price_basic + $this->price_basic * $margin)
        );
    }

    public function isPromo() {
        return $this->basicPriceBrutto() > $this->priceBrutto();
    }

    public function url() {
        return '/produkt/' . Inflector::slug(TransliteratorHelper::process($this->name, '', 'en'), '_') . '-' . $this->id;
    }

    public function getGuarantee() {
        return $this->hasOne(ShopGuarantee::className(), ['id' => 'warranty']);
    }

    public function getProducer() {
        return $this->hasOne(Producer::className(), ['producer_id' => 'producer_id']);
    }

    public function getImages() {
        return $this->hasMany(ShopProductImage::className(), ['product_id' => 'id']);
    }

    public function getProductAttributes() {
        return $this->hasMany(ShopProductAttribute::className(), ['product_id' => 'id']);
    }

    public function getProductAttributesSort() {
        return $this->hasMany(ShopProductAttribute::className(), ['product_id' => 'id'])->joinWith(['attributeName'])->orderBy('position asc');
    }

}

<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "setting".
 *
 * @property string $id
 * @property string $name
 * @property string $value
 * @property string $description
 * @property string $field_type
 * @property string $category
 */
class Setting extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'setting';
    }

    public static function getFilterCategory()
    {
        return ArrayHelper::map(Setting::find()->groupBy('category')->all(),'category','category');
    }

    /**
     * __toString()
     *
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['description'], 'string'],
            [['name', 'value', 'field_type', 'category'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Nazwa'),
            'value' => Yii::t('app', 'Wartość'),
            'description' => Yii::t('app', 'Opis'),
            'field_type' => Yii::t('app', 'Typ'),
            'category' => Yii::t('app', 'Grupa'),
        ];
    }
}

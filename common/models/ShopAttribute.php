<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "shop_attribute".
 * @property string $id
 * @property string $name
 */
class ShopAttribute extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'shop_attribute';
    }

    /**
     * __toString()
     * @return string
     */
    public function __toString() {
        return $this->name;
    }

    public static function getFilterList($category = null) {

        return ArrayHelper::map(self::find()
          ->orderBy('name asc')
          ->andFilterWhere(['show_category'=>$category])
          ->orderBy('position asc')
          ->asArray()
          ->all(), 'id', 'name');
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
}

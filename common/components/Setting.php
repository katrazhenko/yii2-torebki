<?php

namespace common\components;

use Yii;
use yii\base\Component;

class Setting extends Component
{

    public $table = 'setting';

    public function get($name)
    {
        $result = Yii::$app->getDb()->createCommand("SELECT * FROM {{%".$this->table."}} WHERE [[name]]=:name LIMIT 1", [':name' => $name])->queryOne();

        if (!$result) {
            $this->addToDB('default', $name, '');
        } else {
            return $result['value'];
        }

        return $this->get($name);
    }

    public function mailer()
    {
        $mailer = Yii::$app->mailer;
        $mailer->transport = [
            'class' => 'Swift_SmtpTransport',
            'host' => $this->get('smtp_host'),
            'username' => $this->get('smtp_login'),
            'password' => $this->get('smtp_pass'),
            'port' => '25',
          //  'encryption' => 'tls',
        ];

        $mailer->useFileTransport = false;

        return $mailer;
    }

    protected function addToDB($category, $name, $value)
    {
        Yii::$app->getDb()->createCommand(
            "INSERT INTO {{%".$this->table."}} ([[category]], [[name]], [[value]]) VALUES(:cat,:name,:value)",
            [
                ':cat' => $category,
                ':name' => $name,
                ':value' => $value,
            ])->execute();
    }


}

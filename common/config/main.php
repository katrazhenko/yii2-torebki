<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'setting' => [
            'class' => 'common\components\Setting',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'i18n' => [
            'translations' => [
                'app' => [
                        'class' => 'yii\i18n\PhpMessageSource',
                        'basePath' => '@common/messages',
                        'fileMap' => [ 'app' => 'app.php', ]
                        ]
                ]
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\DbTarget',
                    'levels' => ['error']
                ],
                [
                    'class' => 'yii\log\EmailTarget',
                    'levels' => ['error'],
                    'mailer' =>'mailer',
                    'except' => ['yii\web\HttpException:404',],
                    'message' => [
                        'from' => ['error@pceuropa.net'],
                        'to' => ['error@pceuropa.net'],
                        'subject' => 'torebki',
                    ],
                ],
            ],
        ],
    ],
    'modules' => [
        'yii2images' => [
            'class' => 'rico\yii2images\Module',
            'imagesStorePath' => '@app/web/images/store', //path to origin images
            'imagesCachePath' => '@app/web/images/cache', //path to resized copies
            'graphicsLibrary' => 'Imagick', //but really its better to use 'Imagick'
        ],
        'cms' => [
            'class' => 'common\modules\cms\Cms',
        ],
    ],
    'params' => [
        'adminEmail' => [],
    ],
];
